#!/bin/bash
# set -e

DEBUG=false
EXTRA_DEBUG_INFO=false
AUTOREBOOT=false

#======================================| If debugging, then turn on debug output
# if [[ ${DEBUG} == true ]] && [[ ${EXTRA_DEBUG_INFO} == true ]]; then set -x -v; fi

#======================================| USERS & PASSWORDS
OVERRIDE_UBUNTU_SECURITY=true # @TODO: at the moment drush qc doesn't work without this.
USER_USER=${USER}             # @TODO: prompt user to enter user/passwords
USER_PASS=${USER}
MYSQL_USER=${USER}
MYSQL_PASS=${USER}

#======================================| SERVER SETTINGS
WWW_FQDN="localhost"          # used by apache setup
WWW_SERVER="apache2"          # webserver to install (used by phpmyadmin)
SQL_SERVER="mariadb"          # allowed values: mysql, mariadb, percona @TODO: add sqlite
INSTALL_PHP56=false           # Install legacy PHP version change to true to install

#======================================| PATHS
HOME_ROOT="${HOME}/websites"      # Fullpath to where websites info will be installed
WWW_ROOT="/var/www/vhosts"        # Fullpath to where websites will be installed
LOGS="${HOME_ROOT}/logs"          # Fullpath to where symlink to LAMP logfiles will be stored
CONFIGS="${HOME_ROOT}/config"     # Fullpath to where symlink to LAMP config will be stored
APP_FOLDER="/usr/share"           # Fullpath installed user APPLICATIONS & SCRIPTS
HOSTSHARE="shared"                # FOLDER NAME of share to access Virtualbox (host) files

#======================================| Default Ubuntu apps that can be purged
PURGE_OFFICE=true             # Libre Office
PURGE_GAMES=true              # Standard Games
PURGE_BLUETOOTH=true          # Bluetooth support
PURGE_MEDIA=true              # Audio and Video applications and unity lens'
PURGE_MISC=true               # Various: screensaver, speech synthesis, bittorrent, ubuntuone, remote desktop, USB creator, driver UI,
PURGE_ASYNC_COMM=true         # Async communications: microblog, email
PURGE_RT_COMM=false           # Realtime communications: IM, VOIP, IRC
PURGE_HELP=false              # Help documents
PURGE_PRINT=false             # Printing system
PURGE_ACCESSIBILITY=false     # This should be false if making an image for the public.

#======================================| Add Optional Applications
# LAMP Developer tools
INSTALL_DRUSH=false           # Install and configure Drush
INSTALL_XDEBUG=false          # Used for PHP debuging
INSTALL_EMAIL_CATCHER=false   # Redirects PHP email requests to a file. Prevents accidentally sending email while developing.
INSTALL_WEBGRIND=false        # Requires XDEBUG. Webgrind is an Xdebug profiling web frontend in PHP5. It implements a subset of the features of kcachegrind and installs in seconds and works on all platforms. For quick'n'dirty optimizations it does the job.
INSTALL_XHPROF=false          # XHProf is a function-level hierarchical profiler for PHP and has a simple HTML based user interface. @FIXME: not currently working?
INSTALL_TIDEWAYS=false        # Tideways started as a fork of XHProf and now is the drop in replacement for PHP7 applications
# Editors / IDEs
INSTALL_BLUEFISH=false        # Install BLuefish HTML Editor (Similarities to Dreamweaver)
INSTALL_GEDIT=false           # Configure Gedit
INSTALL_SUBLIME=false         # NONFREE/TRIALWARE: Install and configure Sublime Text
INSTALL_PHPSTORM=true         # NONFREE/TRIALWARE: Install and configure PHPStorm Text
INSTALL_GEANY=false           # Install and configure Geany
INSTALL_ECLIPSE=false         # Install Eclipse, a JAVA IDE that supports XDEBUG
INSTALL_APTANA=false          # Install Aptana, a JAVA IDE, PHP/Python/Ruby/Web focussed commercial fork of eclipse.  @FIXME - may have SWT errors related to current JAVA stack
INSTALL_NETBEANS=false        # Install Nebeans, a JAVA IDE that support XDEBUG
INSTALL_VSCODE=false          # Install VS Code, a Microsoft IDE that support XDEBUG
# Graphics / Theming
INSTALL_GIMP=false            # Install gimp and icc-profiles-free
INSTALL_INKSCAPE=false        # Install inkscape
INSTALL_COMPASS=false         # Install Compass
INSTALL_GRAPHIC_XTRAS=false   # Install Ardesia and Graphics Lens
# Developer Utilities
INSTALL_POWER_UTILS=false     # Install Diodon clipboard, Autokey, xchat, bleachbit, synaptic
INSTALL_GIT_POWER=true        # Install GIT GUI, gitg, gitk, meld, nautilus-compare
INSTALL_TERMINAL_UTILS=false   # Install grsync, nautilus-open-in-terminal, guake
INSTALL_EXTRA_INDICATORS=false # Install additional PPA's to enable indicators for: system load, recent notifications, FluxGui, google tasks, google drive
# Extra Configuration
INSTALL_ETCKEEPER=false       # Keeps /etc/ folder under version control in order to track whats changed since vanilla setup.
REMOVE_DEFAULT_FOLDERS=true   # Remove default Ubuntu folders
INSTALL_FIREWALL=false        # Install and setup Firewall
INSTALL_JRE=false             # Optional. If needed JAVA will automatically be installed by Netbeans, Eclipse or Aptana
ADD_NAUTILUS_EMBLEMS=false    # Add emblems to folders in Nautilus @FIXME: not currently working in a script @FIXME

#======================================| DRUSH version to install
DRUSH_VERSION="8.x"
# Options to download supported versions are:
# "master" - Drush 9 (Supporting D7, D8)
# "8.x" - Drush 8 (Supporting D6, D7, D8)
# "7.x" - Drush 7 (Supporting D6, D7)

# Drush addons
# current version 7.x-1.0-alpha1, but latest branch is
FEATHER="7.x-1.x"

#======================================| Used for network testing during installation
PINGHOST1=google.com
PINGHOST2=wikipedia.org
PINGRESULTS=2

#======================================| WGET variables
REFERER="http://www.drupal.org/project/DrupalPro"
USERAGENT="Mozilla/5.0 (compatible; Konqueror/4.4; Linux 2.6.32-22-generic; X11; en_US) KHTML/4.4.3 (like Gecko) Kubuntu"
HEAD1="Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5"
HEAD2="Accept-Language: en-us,en;q=0.5"
HEAD3="Accept-Encoding: gzip,deflate"
HEAD4="Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7"
HEAD5="Keep-Alive: 300"
# If debugging then turn on WGET debug output
if [[ ${DEBUG} == true ]]; then
  WGET_VERBOSE=""
  if [[ ${EXTRA_DEBUG_INFO} == true ]];then WGET_VERBOSE="-v";fi
  else
  WGET_VERBOSE="-nv"
fi

# XHPROF
XHPROF_URL="https://github.com/facebook/xhprof/archive/master.zip"

# Firefox FEBE profile
FEBE_URL="https://dl.dropbox.com/u/6569361/ddd/profileFx4%7Bddd%7D.fbu"

#====================| IDE RELATED
# Sublime Text 2
if [ `uname -p` == "x86_64" ]
then
  SUBLIME_URL="https://download.sublimetext.com/sublime_text_3_build_3126_x64.tar.bz2"
else
  # 32-bit
  SUBLIME_URL="https://download.sublimetext.com/sublime_text_3_build_3126_x32.tar.bz2"
fi

# PHPStorm
PHPSTORM_OLD_URL="https://download.jetbrains.com/webide/PhpStorm-2023.3.6.tar.gz"
PHPSTORM_OLD_ROOT="PhpStorm-2023.3.6"
PHPSTORM_URL="https://download.jetbrains.com/webide/PhpStorm-2024.1.1.tar.gz"
PHPSTORM_ROOT="PhpStorm-2024.1.1"

# GEANY add-ons
# http://wiki.geany.org/tags/start
GEANY_THEME="https://github.com/downloads/codebrainz/geany-themes/geany-themes-1.22.tar.bz2"
GEANY_DRUPAL="http://wiki.geany.org/_media/tags/drupal.php.tags"
GEANY_PHP="http://wiki.geany.org/_media/tags/phpfull-5.3.5.php.tags"
#GEANY_JS="http://dl.yent.eu/em.js.tags"
GEANY_JS="https://gist.githubusercontent.com/anonymous/c4b83ad64e54806eb28d137bee154387/raw/cd83db867a8eae9c2e30d91457ef1136abd2a350/em.js.tags"
GEANY_CSS="http://wiki.geany.org/_media/tags/standard.css.tags"

# ECLIPSE >> http://www.eclipse.org/pdt/downloads/
if [ `uname -p` == "x86_64" ]
then
  ECLIPSE_URL="https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/oxygen/R/eclipse-php-oxygen-R-linux-gtk-x86_64.tar.gz"
  # ECLIPSE_URL="http://ftp.osuosl.org/pub/eclipse/technology/epp/downloads/release/helios/SR2/eclipse-php-helios-SR2-linux-gtk-x86_64.tar.gz"
  # ECLIPSE_URL="http://zend-sdk.googlecode.com/files/eclipse-php-3.0.2.v20120511142-x86_64.tar.gz"
else
  ECLIPSE_URL="https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/oxygen/R/eclipse-php-oxygen-R-linux-gtk.tar.gz"
  # ECLIPSE_URL="http://ftp.osuosl.org/pub/eclipse/technology/epp/downloads/release/helios/SR2/eclipse-php-helios-SR2-linux-gtk.tar.gz"
  # ECLIPSE_URL="http://zend-sdk.googlecode.com/files/eclipse-php-3.0.2.v20120511142-x86.tar.gz"
fi

# APTANA
if [ `uname -p` == "x86_64" ]
then
  APTANA_URL="https://github.com/aptana/studio3/releases/download/v3.6.1/Aptana_Studio_3_Setup_Linux_x86_64_3.6.1.zip"
else
  APTANA_URL="https://github.com/aptana/studio3/releases/download/v3.6.1/Aptana_Studio_3_Setup_Linux_x86_3.6.1.zip"
fi

# NETBEANS

# Latest Stable Netbeans Full Language Release
#NETBEANS_URL="http://download.netbeans.org/netbeans/8.2/final/bundles/netbeans-8.2-linux.sh"
#NETBEANS_ROOT="netbeans-8.2"
#NETBEANS_DESKTOP="$NETBEANS_ROOT"

# Latest Stable Netbeans PHP Release
# NETBEANS_URL="http://download.netbeans.org/netbeans/8.2/final/bundles/netbeans-8.2-php-linux-x64.sh"
NETBEANS_URL="http://download.netbeans.org/netbeans/8.2/rc/bundles/netbeans-8.2-php-linux-x64.sh"
NETBEANS_ROOT="netbeans-8.2"
NETBEANS_DESKTOP="Netbeans"

# Preferences file for Drupal coding
NETBEANS_PREF="https://dl.dropbox.com/u/6569361/ddd/netbeans-prefs.zip"

# VSCode
VSCODE_URL="https://code.visualstudio.com/sha/download?build=stable&os=linux-x64"

#====================| Cheatsheets for wallpaper
#CHEAT1="https://dl.dropbox.com/u/6569361/ddd/drupal-contribute-by-zaferia.svg"
CHEAT1="https://f.fwallpapers.com/images/drupal-background.png"
#CHEAT2="http://media.smashingmagazine.com/wp-content/uploads/uploader/images/drupal-cheat-sheet-wallpaper/wallpaper1920x1200.png"
CHEAT2="https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/e319d737-0bc1-488f-8ae4-4688d96223bf/wallpaper1920x1200.png"
CHEAT3="http://www.quicklycode.com/wp-content/files/drupal7_1920x1200.jpg"
#CHEAT4="http://www.nihilogic.dk/labs/canvas_sheet/HTML5_Canvas_Cheat_Sheet.png"
CHEAT4="http://78.media.tumblr.com/TXllBKpGnm082k7ls09IJvsUo1_1280.png"
#CHEAT5="http://media.smashingmagazine.com/wp-content/uploads/2010/05/VI-Help-Sheet-01-large2.jpg"
CHEAT5="https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/52dc7b32-ee7c-491a-ac49-12779ff663be/vi-help-sheet-01-large2.jpg"
DEFAULT_BG="${CHEAT1##*/}"

#======================================| AUTOMATIC VARIABLES
# Find folder name of install scripts
# and set project (DDD) root folder
#CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#cd -P "${CWD}"
#cd ..
#export DDD=${PWD##*/}
#export DDD_PATH=${PWD#"$HOME/"}

DDD_PATH="${HOME}/drupalpro"

# If debugging, figure out appropriate APTGET debug output
if [[ ${EXTRA_DEBUG_INFO} == true ]]; then
  APTGET_VERBOSE="-yq"
  else
  APTGET_VERBOSE="-yqq"
fi

stage_finished=99

#======================================| RUNTIME VARIABLES AFTER THIS POINT

# ################## START_USER_CONFIG
INSTALLTYPE=virtual
