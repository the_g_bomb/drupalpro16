#!/bin/bash

#======================================| memcached

# README:
#
# This script will install memcached (a memory object caching system) on port 11211
#
HELP="

Memcached Installation complete.

Memcached is a memory object caching system.  It is running on port 11211.

To restart memcached:  sudo /etc/init.d/memcached restart

For details on using memcached with Drupal, see here: http://drupal.org/project/memcache

To clear memcache use:
echo 'flush_all' | nc localhost 11211
OR
echo 'flush_all' | netcat localhost 11211
OR
nc <<ipaddress>> 11211<<<"flush_all"
"

cd ~
sudo apt-get update

#======================================| Install Memcache
# Install memcached and the PHP5 module
sudo apt-get -y install memcached php-memcached

# Install memcache PECL (PHP Extension Community Library) library.
sudo apt-get -y install php-dev php-pear build-essential > /dev/null
yes "\n" | sudo pecl -q install memcache > /dev/null
echo "extension=memcache.so" | sudo tee /etc/php/7.4/mods-available/memcache.ini > /dev/null

sudo phpenmod memcache

# Restart Apache
sudo /etc/init.d/apache2 restart #use sysvinit scripts instead of upstart for more compatibility (debian, older ubuntu, etc)

echo "$HELP"
