#!/bin/bash
set -e

#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

#======================================| Install LAMP packages
# Define package names, and debconf config values.  Keep package names in sync.
LAMP_APACHE="apache2 libapache2-mod-php"

#======================================| Prepare PHP
LAMP_PHP="php php-dev php-cli php-curl php-gd php-imagick php-imap php-intl php-mbstring php-xml php-sqlite3 php-zip php-soap"
# If needed install mcrypt php-mcrypt use
#https://computingforgeeks.com/install-php-mcrypt-extension-on-ubuntu/
LAMP_PHP="${LAMP_PHP} mcrypt libmcrypt-dev"

#======================================| Prepare MYSQL
if [[ "${SQL_SERVER}" == mysql ]]; then
  LAMP_MYSQL="mysql-server"
  echo mysql-server-5.7 mysql-server/root_password        password ${MYSQL_PASS} | sudo debconf-set-selections
  echo mysql-server-5.7 mysql-server/root_password_again  password ${MYSQL_PASS} | sudo debconf-set-selections
  LAMP_PHP="${LAMP_PHP} php-mysql"
fi

#======================================| Prepare Mariadb
if [[ "${SQL_SERVER}" == mariadb ]]; then
  LAMP_MYSQL="mariadb-server"
  export DEBIAN_FRONTEND=noninteractive
  echo mariadb-server-10.3 mysql-server/root_password        password ${MYSQL_PASS} | sudo debconf-set-selections
  echo mariadb-server-10.3 mysql-server/root_password_again  password ${MYSQL_PASS} | sudo debconf-set-selections
  LAMP_PHP="$LAMP_PHP php-mysql dbconfig-common"
  # # Remove installation
  # sudo apt-get -y --remove purge mariadb-server*
  # sudo apt-get -y autoremove
  # sudo apt-get -y autoclean
  # sudo apt-get -y clean all
  # rm -rf /etc/mysql/ /var/lib/mysql/ /var/log/mysql
  # sudo apt-get -y clean
fi

#======================================| Prepare Percona
if [[ "${SQL_SERVER}" == percona ]]; then

  sudo apt-get install gnupg2
  wget https://repo.percona.com/apt/percona-release_latest.$(lsb_release -sc)_all.deb
  sudo dpkg -i percona-release_latest.$(lsb_release -sc)_all.deb
  sudo apt-get update &
  wait

  LAMP_MYSQL="percona-server-server-5.7"
  echo percona-server-server-5.7 percona-server-server/root_password        password ${MYSQL_PASS} | sudo debconf-set-selections
  echo percona-server-server-5.7 percona-server-server/root_password_again  password ${MYSQL_PASS} | sudo debconf-set-selections
  LAMP_PHP="${LAMP_PHP} php-mysql"
fi

#======================================| Prepare PHPMyAdmin
LAMP_TOOLS="phpmyadmin"
echo phpmyadmin       phpmyadmin/reconfigure-webserver  text     ${WWW_SERVER}    | sudo debconf-set-selections
echo phpmyadmin       phpmyadmin/dbconfig-install       boolean  true       | sudo debconf-set-selections
echo phpmyadmin       phpmyadmin/app-password-confirm   password ${USER_PASS} | sudo debconf-set-selections  # @TODO need to confirm user and passowrd is set correctly
echo phpmyadmin       phpmyadmin/mysql/admin-pass       password ${MYSQL_PASS} | sudo debconf-set-selections  # @TODO need to confirm user and passowrd is set correctly
echo phpmyadmin       phpmyadmin/password-confirm       password ${USER_PASS} | sudo debconf-set-selections  # @TODO need to confirm user and passowrd is set correctly
echo phpmyadmin       phpmyadmin/setup-password         password ${USER_PASS} | sudo debconf-set-selections  # @TODO need to confirm user and passowrd is set correctly
echo phpmyadmin       phpmyadmin/mysql/app-pass         password ${MYSQL_PASS} | sudo debconf-set-selections  # @TODO need to confirm user and passowrd is set correctly

# Now install the packages.  debconf shouldn't need to ask so many questions.
sudo apt-get ${APTGET_VERBOSE} install ${LAMP_APACHE} ${LAMP_MYSQL} ${LAMP_PHP}
sudo apt-get ${APTGET_VERBOSE} install ${LAMP_TOOLS}

sudo mysql_secure_installation

# ###### Configure APACHE
echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/fqdn.conf

sudo a2enconf fqdn
sudo a2enmod headers
sudo a2enmod ssl
sudo a2enmod rewrite

# configure default site
echo "<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html
        ErrorLog \${APACHE_LOG_DIR}/error.log
        CustomLog \${APACHE_LOG_DIR}/access.log combined
        <Directory /var/www/html>
          Options Indexes FollowSymLinks MultiViews
          AllowOverride All
          Order allow,deny
          allow from all
        </Directory>
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
" | sudo tee /etc/apache2/sites-available/000-default.conf  > /dev/null

sudo a2ensite 000-default

# Fix ssl for easier virtual hosting
# echo "NameVirtualHost *:80
# Listen 80
# <IfModule mod_ssl.c>
#     NameVirtualHost *:443
#     Listen 443
# </IfModule>" | sudo tee /etc/apache2/ports.conf > /dev/null

# echo "
# Listen 80
# <IfModule ssl_module>
# 	Listen 443
# </IfModule>
# <IfModule mod_gnutls.c>
# 	Listen 443
# </IfModule>" | sudo tee /etc/apache2/ports.conf > /dev/null

echo "<IfModule mod_ssl.c>
        <VirtualHost _default_:443>
                ServerAdmin webmaster@localhost
                DocumentRoot /var/www/html
                <Directory />
                    Options FollowSymLinks
                    AllowOverride None
                </Directory>
                <Directory /var/www/html>
                    Options Indexes FollowSymLinks MultiViews
                    AllowOverride None
                    Order allow,deny
                    allow from all
                </Directory>
                ErrorLog ${APACHE_LOG_DIR}/error.log
                CustomLog ${APACHE_LOG_DIR}/access.log combined
                SSLEngine on
                SSLCertificateFile    /etc/ssl/certs/ssl-cert-snakeoil.pem
                SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key
                <FilesMatch "\.(cgi|shtml|phtml|php)$">
                                SSLOptions +StdEnvVars
                </FilesMatch>
                <Directory /usr/lib/cgi-bin>
                                SSLOptions +StdEnvVars
                </Directory>
        </VirtualHost>
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
" | sudo tee /etc/apache2/sites-available/default-ssl.conf > /dev/null

sudo a2ensite default-ssl

#======================================| Configure MYSQL

if [[ "${SQL_SERVER}" == percona ]]; then
  sudo sed -i 's/#log_slow_queries/slow_query_log_file/g'          /etc/mysql/my.cnf
  echo "
slow_query_log                 = 1
" | sudo tee -a /etc/mysql/my.cnf > /dev/null
fi

if [[ "${SQL_SERVER}" == mariadb ]]; then
echo "
[mariadb]
# Slow query log configuration.
slow_query_log
slow_query_log_file=/var/log/mysql/mariadb-slow.log
long_query_time=2
" | sudo tee -a /etc/mysql/mariadb.conf.d/50-slowquery.cnf > /dev/null

echo "
[mariadb]
# InnoDB settings.
# innodb_large_prefix = 1
# innodb_file_format = barracuda
innodb_file_per_table = 1
innodb_buffer_pool_size = 256M
innodb_log_file_size = 64M
innodb_log_buffer_size = 8M
innodb_flush_log_at_trx_commit = 1
innodb_lock_wait_timeout = 50
" | sudo tee -a /etc/mysql/mariadb.conf.d/50-innodb.cnf > /dev/null
fi

if [[ "${SQL_SERVER}" == mysql ]]; then
echo "
[mysqld]
# Slow query log configuration.
slow_query_log = 1
slow_query_log_file = /var/log/mysql/mysql-slow.log
long_query_time = 2
" | sudo tee -a /etc/mysql/mysql.conf.d/slowquery.cnf > /dev/null
fi

# @TODO add 4 byte UTF-8 for mysql

echo "
[mysqld]
# InnoDB settings.
# innodb_large_prefix = 1
# innodb_file_format = barracuda
innodb_file_per_table = 1
innodb_buffer_pool_size = 256M
innodb_log_file_size = 64M
innodb_log_buffer_size = 8M
innodb_flush_log_at_trx_commit = 1
innodb_lock_wait_timeout = 50
" | sudo tee -a /etc/mysql/mysql.conf.d/innodb.cnf > /dev/null

#======================================| Configure PHP

echo "
; custom overrides for php settings
; priority=30
;;;;;;;;;;;;;;;;;;;;
; Language Options ;
;;;;;;;;;;;;;;;;;;;;
short_open_tag = Off
;;;;;;;;;;;;;;;;;;;
; Resource Limits ;
;;;;;;;;;;;;;;;;;;;
max_execution_time = 300
max_input_time = 60
max_input_vars = 4000
memory_limit = 512M
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Error handling and logging ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
display_errors = On
display_startup_errors = On
error_log = \/var\/log\/php-errors.log
;;;;;;;;;;;;;;;;;
; Data Handling ;
;;;;;;;;;;;;;;;;;
post_max_size = 50M
;;;;;;;;;;;;;;;;;
; Data Handling ;
;;;;;;;;;;;;;;;;;
upload_max_filesize = 64M
" | sudo tee -a /etc/php/7.4/mods-available/overrides.ini > /dev/null

echo "
; custom overrides for php cli settings
; priority=40
memory_limit = -1
" | sudo tee -a /etc/php/7.4/mods-available/overrides-cli.ini > /dev/null

sudo phpenmod imap
sudo phpenmod overrides
sudo phpenmod -s cli overrides-cli

#======================================| Install upload progress (warning in D7)
sudo apt-get ${APTGET_VERBOSE} install php-uploadprogress
sudo phpenmod uploadprogress

#======================================| Log Files

mkdir -p "${LOGS}"

# Apache error logs are configured in the VirtualHosts section of each website (default from apache2.conf)
sudo touch     /var/log/apache2/error.log
sudo chmod g+w /var/log/apache2/error.log
ln -s          /var/log/apache2/error.log                "${LOGS}/apache-error.log"
# This file catches any unconfigured log info for virtualhosts (default from apache2.conf)
sudo touch     /var/log/apache2/other_vhosts_access.log
sudo chmod g+w /var/log/apache2/other_vhosts_access.log
ln -s          /var/log/apache2/other_vhosts_access.log  "${LOGS}/apache-access.log"
# php error logs are configured in php.ini  (changed in install-3-lamp.sh)
sudo touch     /var/log/php-errors.log
sudo chmod g+w /var/log/php-errors.log
ln -s          /var/log/php-errors.log                   "${LOGS}/php-errors.log"
# MySQL error and slow query logs (changed in install-2-lamp.sh)
sudo touch     /var/log/mysql/error.log
sudo chmod g+w /var/log/mysql/error.log
ln -s          /var/log/mysql/error.log                  "${LOGS}/mysql-error.log"
sudo touch     /var/log/mysql/mysql-slow.log
sudo chmod g+w /var/log/mysql/mysql-slow.log
ln -s          /var/log/mysql/mysql-slow.log             "${LOGS}/mysql-slow.log"



#======================================| Config Files
mkdir -p "${CONFIGS}"
sudo chmod -R g+w /etc/apache2
ln -s /etc/apache2/apache2.conf      "${CONFIGS}/apache2.conf"
ln -s /etc/apache2/httpd.conf        "${CONFIGS}/httpd.conf"
ln -s /etc/apache2/ports.conf        "${CONFIGS}/ports.conf"
ln -s /etc/apache2/sites-enabled/    "${CONFIGS}/apache-sites-enabled"
sudo chmod -R g+w /etc/php
ln -s /etc/php/7.0/apache2/php.ini   "${CONFIGS}/php-apache.ini"
ln -s /etc/php/7.0/cli/php.ini       "${CONFIGS}/php-cli.ini"
sudo chmod -R g+w /etc/mysql
ln -s /etc/mysql/my.cnf              "${CONFIGS}/mysql.cnf"
ln -s /etc/mysql/mariadb.conf.d/     "${CONFIGS}/maria-conf-d"
sudo chmod g+w /etc/hosts
ln -s /etc/hosts                     "${CONFIGS}/hosts"

echo "This folder contains links (shortcuts) to LAMP configuration files.  This was
created to make it easier for new users to find logs. However, all Ubuntu/Debian
servers (and other flavors of linux) store configuration in the /etc/ folder.

To see the links, and the path to the config files, open a terminal (F4) & type:

ll

This will list the files and where they link to." > "${CONFIGS}/README.txt"

#======================================| Drupal sites
# Create folder for websites to live in
sudo mkdir -p "${WWW_ROOT}"
sudo chown "${USER}":www-data "${WWW_ROOT}"
sudo chmod -R ug=rwX,o= "${WWW_ROOT}"

#======================================| user management
# Make user of group www-data
sudo adduser "${USER}" www-data

# ###### Restart web server

sudo /etc/init.d/mysql restart      #use sysvinit scripts instead of upstart for more compatibility (debian, older ubuntu, etc)
sudo /etc/init.d/apache2 restart    #use sysvinit scripts instead of upstart for more compatibility (debian, older ubuntu, etc)

stage_finished=0
exit "$stage_finished"
