# API
# --------
api: 2

# Core
# --------
core: 8.x

# Projects
# --------
# Specify common subdir of "contrib"
defaults:
  projects:
    subdir: "contrib"
    version: ~

projects:
  # Core
  drupal:
    version: ~

  # Contrib
  admin_toolbar:
    version: ~
  ctools:
    version: ~
  devel:
    version: ~
  google_analytics:
    version: ~
  metatag:
    version: ~
  module_filter:
    version: ~
  pathauto:
    version: ~
  token:
    version: ~

  # Themes
  basic:
    type: "theme"
    subdir: ""
    # Specify a version
    version: "1.x-dev"
