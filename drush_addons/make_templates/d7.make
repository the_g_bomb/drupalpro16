core = 7.x
api = 2

projects[drupal][type] = core
defaults[projects][subdir] = contrib

# Admin
projects[] = admin_menu
projects[] = module_filter

# Site Tools
projects[] = ctools
projects[] = metatag
projects[] = pathauto
projects[] = token
projects[] = views
projects[] = transliteration
projects[] = imagecache_token

# Development
projects[] = devel
