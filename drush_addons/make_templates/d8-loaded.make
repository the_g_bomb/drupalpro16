core: 8.x
api: 2
projects:
  drupal:
    version: 8.2.3
  admin_toolbar:
    version: '1.18'
  ctools:
    version: 3.0-alpha27
  devel:
    version: 1.0-beta1
  google_analytics:
    version: '2.1'
  metatag:
    version: 1.0-beta12
  pathauto:
    version: 1.0-beta1
  token:
    version: 1.0-rc1
  basic:
    version: '1.2'
