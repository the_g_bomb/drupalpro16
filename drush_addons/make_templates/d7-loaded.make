core = 7.x
api = 2

projects[drupal][type] = core

defaults[projects][subdir] = contrib

# Admin
projects[] = admin_menu
projects[] = module_filter

# Site Tools
projects[] = ctools
projects[] = token
projects[] = views
projects[] = transliteration
projects[] = imagecache_token
projects[] = mollom
projects[] = scheduler
projects[] = read_more

# Development
projects[] = devel

# SEO
projects[] = metatag
projects[] = metatags_quick
projects[] = pathauto
projects[] = globalredirect
projects[] = redirect
projects[] = google_analytics
projects[] = ga_tokenizer
projects[] = contact_google_analytics
projects[] = context_keywords
projects[] = microdata
projects[] = htmlpurifier
projects[] = search404
projects[] = site_verify
projects[] = xmlsitemap
projects[] = site_map

# Checklists
projects[] = checklistapi
projects[] = seo_checker
projects[] = seo_checklist
projects[] = security_review

# Libraries
libraries[htmlpurifier][download][type] = "file"
libraries[htmlpurifier][download][url] = "http://htmlpurifier.org/releases/htmlpurifier-4.9.3.zip"
