#!/bin/bash
set -e

#======================================| Import Variables
# Make sure you have edited this file
# CWD based on http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

sudo apt-get ${APTGET_VERBOSE} update
sudo apt-get ${APTGET_VERBOSE} upgrade
sudo apt-get ${APTGET_VERBOSE} autoremove # autoremove is used to remove packages that were automatically installed to satisfy dependencies for other packages and are now no longer needed.
sudo apt-get ${APTGET_VERBOSE} clean # clean clears out the local repository of retrieved package files. It removes everything but the lock file from /var/cache/apt/archives/ and /var/cache/apt/archives/partial/.

#cd ${HOME}/${DDD_PATH}
#git pull
#bash "updates.inc"

stage_finished=0
exit "$stage_finished"
