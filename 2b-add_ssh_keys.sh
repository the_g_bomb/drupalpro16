#!/bin/bash
set -e

#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

echo "Preparing ssh, please wait..."

mkdir -p ~/.ssh
mv ~/.ssh ~/.ssh_bak
cp -R ~/.ssh_shared ~/.ssh
sudo chown -R ${USER}:${USER} ~/.ssh/*
chmod 600 ~/.ssh/*
chmod 700 ~/.ssh
eval "$(ssh-agent -s)"
# ssh-add ~/.ssh/<id_rsa_key_name>
echo ""
echo "ssh keys copied and permissions set, please test them now."
echo "  $ ssh -T git@bitbucket.org"
echo "  $ ssh -T git@github.com"
echo "  $ ssh -T git@git.drupal.org"
echo ""
echo "Please also check your config file, it should look like:"
echo "  $ cat ~/.ssh/config"
echo ""
echo "  Host *.acquia-sites.com"
echo "     HostKeyAlgorithms=+ssh-rsa"
echo "     PubkeyAcceptedKeyTypes=+ssh-rsa"
echo ""
echo ""
echo "  Host *.ds.local"
echo "     User git"
echo "     PubkeyAcceptedAlgorithms +ssh-rsa"
echo "     HostkeyAlgorithms +ssh-rsa"
echo ""
echo "     Host *"
echo "  AddKeysToAgent yes"
echo "  IdentityFile ~/.ssh/id_rsa"
echo ""

echo ""
echo "After you have completed this. Please run:"
echo "$ sudo reboot now"
echo "When restarted, please continue with:"
echo "$ ./2c-install_certificates.sh"

stage_finished=0
exit "$stage_finished"
