#!/bin/bash
set -e

#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

echo "Removing snap, please wait..."

## Remove snap and replace Firefox

# https://www.debugpoint.com/remove-snap-ubuntu/
snap list
sudo snap remove --purge firefox
sudo snap remove --purge snap-store
sudo snap remove --purge gnome-3-38-2004
sudo snap remove --purge gnome-42-2204
sudo snap remove --purge gtk-common-themes
sudo snap remove --purge snapd-desktop-integration
sudo snap remove --purge bare
sudo snap remove --purge core20
sudo snap remove --purge core22
sudo snap remove --purge snapd
snap list

sudo apt-get ${APTGET_VERBOSE} remove --autoremove snapd

echo "
Package: snapd
Pin: release a=*
Pin-Priority: -10
" | sudo tee /etc/apt/preferences.d/nosnap.pref > /dev/null

sudo apt-get ${APTGET_VERBOSE} update
sudo apt-get ${APTGET_VERBOSE} install --install-suggests gnome-software
sudo add-apt-repository -y ppa:mozillateam/ppa
sudo apt-get ${APTGET_VERBOSE} update
sudo apt-get ${APTGET_VERBOSE} install -t 'o=LP-PPA-mozillateam' firefox
echo 'Unattended-Upgrade::Allowed-Origins:: "LP-PPA-mozillateam:${distro_codename}";' | sudo tee /etc/apt/apt.conf.d/51unattended-upgrades-firefox
echo "
Package: firefox*
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 501
" | sudo tee /etc/apt/preferences.d/mozillateamppa > /dev/null

# # Revert back to Snap in Ubuntu
echo "To revert to using snap if required:"
echo "$ sudo rm /etc/apt/preferences.d/nosnap.pref"
echo "$ sudo apt update && sudo apt upgrade"
echo "$ sudo snap install snap-store"
echo "$ sudo apt install firefox"
echo ""
echo ""
echo "You should now open Firefox and attempt to add certificates as required:"
echo "  Go to: Firefox Settings > Privacy & Security > Certificates > View Certificate > Import > ~/shared/Cisco_Umbrella_Root_CA.cer > Trust this CA to identify web sites > OK"
echo "  Go to: Firefox Settings > Privacy & Security > Certificates > View Certificate > Import > (/usr/local/share/ca-certificates/mkcert_development_CA_XXXXXXXXXX) (~/.local/share/mkcert/rootCA.pem) > Trust this CA to identify web sites > OK"

echo ""
echo "After you have completed this. Please run:"
echo "$ ./3a-install_docker.sh"

stage_finished=0
exit "$stage_finished"
