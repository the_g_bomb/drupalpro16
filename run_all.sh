#!/bin/bash

# Based on https://git.drupalcode.org/project/quickstart/-/tree/7.x-1.x?ref_type=heads
# ################################################################################ Reboot functions
function reboot {
  echo "gnome-terminal -x bash -c \"~/shared/drupalpro/run_all.sh $1\" &" >> ~/.profile
  touch ~/shared/drupalpro/drupalpro-install.log
  echo "*** REBOOTING ***" | tee -a ~/shared/drupalpro/drupalpro-install.log
  sleep 2
  sudo reboot now
  exit
}

# Undo any previous reboot script
sed -i 's/gnome-terminal -x bash -c/# deleteme /g' ~/.profile

# ################################################################################ Install it!
# this switch statement handles reboots.
cd ~
case "$1" in
"")
  bash -x ~/shared/drupalpro/0-prep.sh 2>&1 | tee -a ~/shared/drupalpro/drupalpro-install.log
  reboot 1
  ;;
"1")
  bash -x ~/shared/drupalpro/1-update.sh 2>&1 | tee -a ~/shared/drupalpro/drupalpro-install.log
  bash -x ~/shared/drupalpro/2a-shared_drives.sh 2>&1 | tee -a ~/shared/drupalpro/drupalpro-install.log
  reboot 2
  ;;
"2")
  bash -x ~/shared/drupalpro/2b-add_ssh_keys.sh 2>&1 | tee -a ~/shared/drupalpro/drupalpro-install.log
  bash -x ~/shared/drupalpro/2c-install_certificates.sh 2>&1 | tee -a ~/shared/drupalpro/drupalpro-install.log
  bash -x ~/shared/drupalpro/2d-remove_snap.sh 2>&1 | tee -a ~/shared/drupalpro/drupalpro-install.log
  reboot 3
  ;;
"3")
  bash -x ~/shared/drupalpro/3a-install_docker.sh  2>&1 | tee -a ~/shared/drupalpro/drupalpro-install.log
  reboot 4
  ;;
"4")
  bash -x ~/shared/drupalpro/3b-test_docker.sh  2>&1 | tee -a ~/shared/drupalpro/drupalpro-install.log
  reboot 5
  ;;
"5")
  bash -x ~/shared/drupalpro/4-install_ddev.sh  2>&1 | tee -a ~/shared/drupalpro/drupalpro-install.log
  reboot 6
  ;;
"6")
  bash -x ~/shared/drupalpro/5-ides.sh  2>&1 | tee -a ~/shared/drupalpro/drupalpro-install.log
  bash -x ~/shared/drupalpro/6-extras_tools.sh  2>&1 | tee -a ~/shared/drupalpro/drupalpro-install.log
  bash -x ~/shared/drupalpro/7-extras_development.sh  2>&1 | tee -a ~/shared/drupalpro/drupalpro-install.log
  ;;
*)
  echo " *** BAD BAD BAD SOMETHING WENT WRONG!  CALL A DOCTOR! *** "
  ;;
esac
