#!/bin/bash
set -e

#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

echo "This script will offer to install some standard Text Editors and IDEs, please answer Y to anything you wish to install..."

#======================================| Lightweight Test Editors
#======================================| Bluefish
echo ""
read -p "Do you need to install the Bluefish Text Editor and tools? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
  sudo apt-get ${APTGET_VERBOSE} install bluefish bluefish-data bluefish-plugins
fi

#======================================| GEANY
# INSTALL GEANY & SETUP FOR DRUPAL / WEB DEVELOPMENT
# Geany is a small fast IDE (based on scintilla ... also what notepad++ is also built on)
# info: http://en.wikipedia.org/wiki/Geany
echo ""
read -p "Do you need to install the Geany Text Editor and tools? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""

  sudo apt-get ${APTGET_VERBOSE} install geany geany-common geany-plugin-addons
  mkdir -p ${HOME}/.config/geany/tags

  # GEANY: Extra color themes
  cd ~
  wget ${WGET_VERBOSE} -O ${HOME}/geany-themes.tar.bz2 --referer="${REFERER}" --user-agent="${USERAGENT}" --header="${HEAD1}" --header="${HEAD2}" --header="${HEAD3}" --header="${HEAD4}" --header="${HEAD5}" "${GEANY_THEME}"
  tar jxf geany-themes.tar.bz2 && rm geany-themes.tar.bz2
  mv -f geany-themes-1.22/* ${HOME}/.config/geany/
  rm -r geany-themes-1.22

  # GEANY: Install extra tag files
  wget ${WGET_VERBOSE} -O ${HOME}/.config/geany/tags/geany-tags-drupal --referer="${REFERER}" --user-agent="${USERAGENT}" --header="${HEAD1}" --header="${HEAD2}" --header="${HEAD3}" --header="${HEAD4}" --header="${HEAD5}" "${GEANY_DRUPAL}"
  wget ${WGET_VERBOSE} -O ${HOME}/.config/geany/tags/geany-tags-php-5.3.5 --referer="${REFERER}" --user-agent="${USERAGENT}" --header="${HEAD1}" --header="${HEAD2}" --header="${HEAD3}" --header="${HEAD4}" --header="${HEAD5}" "${GEANY_PHP}"
  wget ${WGET_VERBOSE} -O ${HOME}/.config/geany/tags/geany-tags-js --referer="${REFERER}" --user-agent="${USERAGENT}" --header="${HEAD1}" --header="${HEAD2}" --header="${HEAD3}" --header="${HEAD4}" --header="${HEAD5}" "${GEANY_JS}"
  wget ${WGET_VERBOSE} -O ${HOME}/.config/geany/tags/geany-tags-css --referer="${REFERER}" --user-agent="${USERAGENT}" --header="${HEAD1}" --header="${HEAD2}" --header="${HEAD3}" --header="${HEAD4}" --header="${HEAD5}" "${GEANY_CSS}"
fi

#======================================| GEDIT
echo ""
read -p "Do you need to install the GEdit Text Editor and tools? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
  sudo apt-get ${APTGET_VERBOSE} install gedit-plugins

  # Config gedit-2
  gconftool-2 -s /apps/gedit-2/preferences/editor/auto_indent/auto_indent --type=bool true
  gconftool-2 -s /apps/gedit-2/preferences/editor/bracket_matching/bracket_matching --type=bool true
  gconftool-2 -s /apps/gedit-2/preferences/editor/line_numbers/display_line_numbers --type=bool true
  gconftool-2 -s /apps/gedit-2/preferences/editor/current_line/highlight_current_line --type=bool true
  gconftool-2 -s /apps/gedit-2/preferences/editor/right_margin/display_right_margin --type=bool true
  gconftool-2 -s /apps/gedit-2/preferences/editor/wrap_mode/wrap_mode --type=string GTK_WRAP_NONE
  gconftool-2 -s /apps/gedit-2/preferences/editor/tabs/insert_spaces --type=bool true
  gconftool-2 -s /apps/gedit-2/preferences/editor/tabs/tabs_size --type=integer 2
  gconftool-2 -s /apps/gedit-2/preferences/editor/save/auto_save --type=bool true

  # Config gedit in gnome3 / unity
  gsettings set org.gnome.gedit.preferences.editor auto-indent true
  gsettings set org.gnome.gedit.preferences.editor bracket-matching true
  gsettings set org.gnome.gedit.preferences.editor display-line-numbers true
  gsettings set org.gnome.gedit.preferences.editor display-right-margin true
  gsettings set org.gnome.gedit.preferences.editor editor-font 'Monospace 12'
  gsettings set org.gnome.gedit.preferences.editor ensure-trailing-newline true
  gsettings set org.gnome.gedit.preferences.editor highlight-current-line true
  gsettings set org.gnome.gedit.preferences.editor insert-spaces true
  gsettings set org.gnome.gedit.preferences.editor right-margin-position 80
  gsettings set org.gnome.gedit.preferences.editor scheme 'classic'
  gsettings set org.gnome.gedit.preferences.editor syntax-highlighting true
  gsettings set org.gnome.gedit.preferences.editor tabs-size 2
  gsettings set org.gnome.gedit.preferences.editor wrap-mode 'none'
  gsettings set org.gnome.gedit.plugins active-plugins "['bookmarks', 'bracketcompletion', 'codecomment', 'dashboard', 'snippets', 'filebrowser', 'spell', 'modelines', 'colorpicker', 'wordcompletion', 'zeitgeistplugin', 'sessionsaver', 'time', 'docinfo', 'multiedit']"
  gsettings set org.gnome.gedit.plugins.filebrowser enable-remote false
  gsettings set org.gnome.gedit.preferences.ui bottom-panel-visible false
  gsettings set org.gnome.gedit.preferences.ui side-panel-visible true
  gsettings set org.gnome.gedit.preferences.ui statusbar-visible true
fi

#======================================| SUBLIME TEXT 2
echo ""
read -p "Do you need to install the Sublime Text Editor and tools? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
  sudo mkdir -p "${APP_FOLDER}"
  cd "${APP_FOLDER}"
  wget ${WGET_VERBOSE} -O "${HOME}/sublime.tar.bz2" --referer="${REFERER}" --user-agent="${USERAGENT}" --header="${HEAD1}" --header="${HEAD2}" --header="${HEAD3}" --header="${HEAD4}" --header="${HEAD5}" "${SUBLIME_URL}"
  tar jxf "${HOME}/sublime.tar.bz2" && rm "${HOME}/sublime.tar.bz2"
  cd "${APP_FOLDER}/Sublime Text 2"
  chmod u=rwx,o= "${APP_FOLDER}/Sublime Text 2/sublime_text"
  mkdir -p "${HOME}/.local/share/applications"
  cat > "${HOME}/.local/share/applications/Sublime.desktop" <<END
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Sublime Text
GenericName=Text Editor
Comment=Edit source code
Encoding=UTF-8
Exec="${APP_FOLDER}/Sublime Text 2/sublime_text" %F
Icon=${APP_FOLDER}/Sublime Text 2/Icon/48x48/sublime_text.png
Categories=Application;Development;Utility
Version=1.0
Terminal=false
Type=Application
StartupNotify=true
MimeType=text/plain;
Categories=TextEditor;Development;Utility;
END

chmod 750 "${HOME}/.local/share/applications/Sublime.desktop"

cat > "${APP_FOLDER}/Sublime Text 2/README.txt" <<END

NOTICE:
Sublime Text is NOT Open Source software nor is it Freeware.  It was downloaded
and installed as part of DrupalPro so you may easily evaluate it because it is
a popular text editor.  It's just not simple for newbies to install.  You might
also want to search Synaptic or Ubuntu Software Center for other editors.

If you use & like Sublime Text, you should support the product and buy it:

http://www.sublimetext.com/buy


If you don't like it, or prefer to only use FOSS software, please delete it from
your system by deleting the folder or running this command from a terminal:

    rm -R ${APP_FOLDER}/Sublime Text 2



NOTICE FROM THE SUBLIME TEXT WEBSITE:
Sublime Text 2 may be downloaded and evaluated for free, however a license must
be purchased for continued use. There is currently no enforced time limit
for the evaluation.

Please submit feature requests to http://sublimetext.userecho.com/.
For notification about new versions, follow sublimehq on twitter.
END

fi

#======================================| Full IDEs
#======================================| APTANA
echo ""
read -p "Do you need to install the Aptana IDE and tools? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
  sudo mkdir -p "${APP_FOLDER}"
  cd "${APP_FOLDER}"
  wget ${WGET_VERBOSE} -O "${APP_FOLDER}/aptana.zip" --referer="${REFERER}" --user-agent="${USERAGENT}" --header="${HEAD1}" --header="${HEAD2}" --header="${HEAD3}" --header="${HEAD4}" --header="${HEAD5}" "${APTANA_URL}"
  unzip -q aptana.zip && rm aptana.zip
  mkdir -p "${HOME}/.local/share/applications"
  cat > "${HOME}/.local/share/applications/AptanaStudio3.desktop" <<END
#!/usr/bin/env xdg-open
[Desktop Entry]
Type=Application
Name=Aptana Studio 3
Comment=Aptana Integrated Development Environment
Icon=${APP_FOLDER}/Aptana_Studio_3/icon.xpm
Exec=${APP_FOLDER}/Aptana_Studio_3/AptanaStudio3
Terminal=false
Categories=Development;IDE;Java;
END
chmod 750 "${HOME}/.local/share/applications/AptanaStudio3.desktop"
fi

#======================================| ECLIPSE
echo ""
read -p "Do you need to install the Eclipse IDE and tools? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
  sudo mkdir -p "${APP_FOLDER}"
  cd "${APP_FOLDER}"
  wget ${WGET_VERBOSE} -O "${APP_FOLDER}/eclipse.tar.gz" --referer="${REFERER}" --user-agent="${USERAGENT}" --header="${HEAD1}" --header="${HEAD2}" --header="${HEAD3}" --header="${HEAD4}" --header="${HEAD5}" "${ECLIPSE_URL}"
  tar -xvf eclipse.tar.gz && rm eclipse.tar.gz
  if [ -e ${APP_FOLDER}/eclipse/icon.xpm ]; then sudo cp ${APP_FOLDER}/eclipse/icon.xpm /usr/share/pixmaps/eclipse.xpm; fi
  if [ -e ${APP_FOLDER}/eclipse/plugins/org.eclipse.platform_3.6.2.v201102101200/eclipse48.png ]; then sudo cp ${APP_FOLDER}/eclipse/plugins/org.eclipse.platform_3.6.2.v201102101200/eclipse48.png /usr/share/pixmaps/eclipse.png; fi
  if [ -e ${APP_FOLDER}/eclipse-php/configuration/org.eclipse.osgi/bundles/224/1/.cp/icons/eclipse48.png ]; then sudo cp ${APP_FOLDER}/eclipse-php/configuration/org.eclipse.osgi/bundles/224/1/.cp/icons/eclipse48.png /usr/share/pixmaps/eclipse.png; fi
  mkdir -p "${HOME}/.local/share/applications"
  cat > "${HOME}/.local/share/applications/Eclipse.desktop" <<END
#!/usr/bin/env xdg-open
[Desktop Entry]
Type=Application
Name=Eclipse
Comment=Eclipse Integrated Development Environment
Icon=${APP_FOLDER}/eclipse/icon.xpm
Exec=${APP_FOLDER}/eclipse/eclipse
Terminal=false
Categories=Development;IDE;Java;
END
chmod 750 "${HOME}/.local/share/applications/Eclipse.desktop"

fi

#======================================| NETBEANS
echo ""
read -p "Do you need to install the Netbeans IDE and tools? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
  sudo mkdir -p "${APP_FOLDER}"
  cd "${APP_FOLDER}"
  wget ${WGET_VERBOSE} -O "${APP_FOLDER}/netbeans.sh" --referer="${REFERER}" --user-agent="${USERAGENT}" --header="${HEAD1}" --header="${HEAD2}" --header="${HEAD3}" --header="${HEAD4}" --header="${HEAD5}" "${NETBEANS_URL}"
  chmod +x ./netbeans.sh
  bash ./netbeans.sh --silent --nospacecheck
  rm netbeans.sh
  # Test if desktop icon exists, if so then move installation to Applications folder since netbeans installs to HOME by default
  if [[ -f "$HOME/.local/share/applications/${NETBEANS_DESKTOP}.desktop" ]]; then
    # move netbeans to applications folder
    mv "${HOME}/${NETBEANS_ROOT}" "${APP_FOLDER}/${NETBEANS_ROOT}"
    # and update paths in [Desktop Entry]
    sed -i "s|${HOME}/${NETBEANS_ROOT}|${APP_FOLDER}/${NETBEANS_ROOT}|g" "$HOME/.local/share/applications/${NETBEANS_DESKTOP}.desktop"
  fi

  # Download Netbeans preferences used for importing
  wget ${WGET_VERBOSE} -O ${HOME}/Desktop/netbeans-prefs.zip --referer="${REFERER}" --user-agent="${USERAGENT}" --header="${HEAD1}" --header="${HEAD2}" --header="${HEAD3}" --header="${HEAD4}" --header="${HEAD5}" "${NETBEANS_PREF}"
fi

#======================================| PHPSTORM
echo ""
read -p "Do you need to install the PHPStorm IDE and tools? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
  echo "Downloading PHPStorm, please wait ..."
  sudo mkdir -p "${APP_FOLDER}/${PHPSTORM_ROOT}"
  sudo wget ${WGET_VERBOSE} -O "${APP_FOLDER}/PhpStorm.tar.gz" --referer="${REFERER}" --user-agent="${USERAGENT}" --header="${HEAD1}" --header="${HEAD2}" --header="${HEAD3}" --header="${HEAD4}" --header="${HEAD5}" "${PHPSTORM_URL}"
  sudo tar xfz "${APP_FOLDER}/PhpStorm.tar.gz" -C "${APP_FOLDER}/${PHPSTORM_ROOT}" --strip-components 1 && sudo rm "${APP_FOLDER}/PhpStorm.tar.gz"
  sudo mv "${APP_FOLDER}/${PHPSTORM_ROOT}" "${APP_FOLDER}/PhpStorm"
  cd "${APP_FOLDER}/PhpStorm/bin"
  sudo chmod +x ./phpstorm.sh
  bash ./phpstorm.sh
  # cat > "${APP_FOLDER}/${PHPSTORM_ROOT}/DRUPALPRO_README.txt" <<END

# NOTICE:
# PHPStorm is NOT Open Source software nor is it Freeware.  It was downloaded
# as part of DrupalPro so you may easily evaluate it because it is a popular IDE.
# There are numerous tutorials on how to get it to work best for Drupal developmemt.
# You might also want to search Synaptic or Ubuntu Software Center for other editors.

# If you use & like PHP Storm, you should support the product and buy it:

# https://www.jetbrains.com/phpstorm/buy/


# If you don't like it, or prefer to only use FOSS software, please delete it from
# your system by deleting the folder or running this command from a terminal:

    # rm -R "${APP_FOLDER}/${PHPSTORM_ROOT}"



# NOTICE FROM THE PHPStorm WEBSITE:
# PhpStorm includes bundled evaluation license key for a free 30-day trial.

# For support please go to https://intellij-support.jetbrains.com/hc/en-us

# END

sudo ln -s ${APP_FOLDER}/${PHPSTORM_ROOT}/bin/phpstorm.sh /usr/local/bin/phpstorm

  sudo cat > "/usr/share/applications/jetbrains-phpstorm.desktop" <<END

[Desktop Entry]
Version=1.0
Type=Application
Name=PhpStorm
Icon=${APP_FOLDER}/${PHPSTORM_ROOT}/bin/phpstorm.svg
Exec="${APP_FOLDER}/${PHPSTORM_ROOT}/bin/phpstorm.sh" %f
Comment=Lightning-smart PHP IDE
Categories=Development;IDE;
Terminal=false
StartupWMClass=jetbrains-phpstorm
StartupNotify=true
END
sudo chmod 750 "/usr/share/applications/jetbrains-phpstorm.desktop"

fi

#======================================| VSCODE
echo ""
read -p "Do you need to install the Visual Studio IDE and tools? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""

  sudo apt-get ${APTGET_VERBOSE} install wget gpg
  wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
  sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
  sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
  rm -f packages.microsoft.gpg

  sudo apt-get ${APTGET_VERBOSE} install apt-transport-https
  sudo apt-get update
  sudo apt-get ${APTGET_VERBOSE} install code # or code-insiders
fi

echo ""
echo "Complete."
echo "This is a work in progress. Please help out where you can."
echo "Incomplete and not fully tested, but to install some extra dev tools, please run:"
echo "$ ./6-extras_tools.sh"

stage_finished=0
exit "$stage_finished"
