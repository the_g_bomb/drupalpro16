#!/bin/bash

# Download Ubuntu Desktop ISO from:
# https://ubuntu.com/download/desktop/thank-you?version=22.04.3&architecture=amd64
# e.g: https://releases.ubuntu.com/22.04.3/ubuntu-22.04.3-desktop-amd64.iso?_ga=2.222921241.342556869.1705069738-1636726850.1704206951

# Download Certificates from:
# Cisco Umbrella CA certificate
# https://static.caci.co.uk/umbrella/Cisco_Umbrella_Root_CA.cer
# Pre-made bundle (based of the Mozilla one as above)
# https://static.caci.co.uk/umbrella/ca-bundle.crt
# https://static.caci.co.uk/umbrella/cacert.crt
# https://static.caci.co.uk/umbrella/cacert.pem
# https://static.caci.co.uk/umbrella/cert.pem

# Add them to the shared directory

# Create a New Environment with
# Skip unattended Installation ticked
#
# 16384 MB Base Memory
# 2 Processors
# 25GB Hard Drive

# Add shared folders from:
# C:/Users/${USER}/.acquia
# C:/Users/${USER}/.aws
# C:/Users/${USER}/.ssh
# C:/Users/${USER}/shared
# C:/Users/${USER}/sites

# Try or Install Ubuntu
# Install Ubuntu
# English UK Keyboard
# Minimal Installation
# Choose Timezone

# In Virtual Box Environment Window choose Devices and then change:
# Shared Clipboard > BiDirectional
# Drag and Drop > BiDirectional

# Check I was on the developer network
# gpresult -r -v |Select-String -Pattern "AnyConnect"
# AnyConnect_Dev

# One other thing I have had to do is:
# rm -f /etc/resolv.conf
# ln -sv /run/systemd/resolve/resolv.conf /etc/resolv.conf

echo "${USER} ALL=(ALL) NOPASSWD: ALL" | sudo tee -a "/etc/sudoers.d/${USER}" > /dev/null
sudo chmod 0440 "/etc/sudoers.d/${USER}"
sudo adduser ${USER} root

sudo apt-get -yq update
sudo apt-get -yq upgrade
sudo apt-get -yq autoremove
sudo apt-get -yq install build-essential ca-certificates curl file git gnupg libnss3-tools mkcert procps wget

PS3='Please select your Virtual environment: '
options=("VirtualBox" "VMWare" "or Quit")
select opt in "${options[@]}"
do
  case $opt in
    "VirtualBox")
      echo "you chose choice $REPLY which is $opt"
      echo "  : We will mount the shared drives as per VirtualBox."
      export INSTALL_VIRTUALBOX=true
      ;;
    "VMWare")
      echo "you chose choice $REPLY which is $opt"
      echo "  : We will mount the shared drives as per VMWare."
      export INSTALL_VMWARE=true
      ;;
    "or Quit")
      break
      ;;
    *) echo "invalid option $REPLY";;
  esac

done

if [[ ${INSTALL_VIRTUALBOX} == true ]]; then
  # VirtualBox
  # https://chewett.co.uk/blog/2610/using-virtualbox-shared-folder-on-ubuntu-20-04/#:~:text=Configuring%20Shared%20Folders%20in%20Virtualbox,folder%20by%20pressing%20the%20%2B%20button.
  echo "acquia /home/${USER}/.acquia/ vboxsf defaults,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
  echo "aws /home/${USER}/.aws/ vboxsf defaults,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
  echo "shared /home/${USER}/shared/ vboxsf defaults,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
  echo "sites /home/${USER}/sites/ vboxsf defaults,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
  echo "ssh /home/${USER}/.ssh_shared/ vboxsf defaults,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
fi

if [[ ${INSTALL_VMWARE} == true ]]; then
  # VMWare
  # https://docs.vmware.com/en/VMware-Workstation-Pro/17/com.vmware.ws.using.doc/GUID-AB5C80FE-9B8A-4899-8186-3DB8201B1758.html
  echo ".host:/acquia /home/${USER}/.acquia/ vboxsf defaults,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
  echo ".host:/aws /home/${USER}/.aws/ fuse.vmhgfs-fuse defaults,allow_other,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
  echo ".host:/shared /home/${USER}/shared/ fuse.vmhgfs-fuse defaults,allow_other,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
  echo ".host:/sites /home/${USER}/sites/ fuse.vmhgfs-fuse defaults,allow_other,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
  echo ".host:/ssh /home/${USER}/.ssh_shared/ fuse.vmhgfs-fuse defaults,allow_other,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
fi

mkdir -p ~/.ssh
cp ~/.ssh_shared/* ~/.ssh/
# cp ~/.ssh_shared/config ~/.ssh/
# cp ~/.ssh_shared/id_opigo ~/.ssh/
# cp ~/.ssh_shared/id_rsa* ~/.ssh/
sudo chown -R ${USER}:${USER} ~/.ssh/*
chmod 600 ~/.ssh/*
chmod 700 ~/.ssh
eval "$(ssh-agent -s)"
# ssh-add ~/.ssh/<id_rsa_key_name>

# ssh -T git@bitbucket.org
# ssh -T git@github.com
# ssh -T git@git.drupal.org
# ssh -T {{project_name}}@{{repo-id}}.prod.hosting.acquia.com e.g. # ssh -T sanctuary.prod@sanctuary.ssh.prod.acquia-sites.com


mkcert -install
sudo cp /home/${USER}/shared/Cisco_Umbrella_Root_CA.cer /usr/local/share/ca-certificates/Cisco_Umbrella_Root_CA.crt
sudo update-ca-certificates

cp /usr/local/share/ca-certificates/Cisco_Umbrella_Root_CA.crt .ddev/web-build/
# cp ~/shared/Cisco_Umbrella_Root_CA.crt .ddev/web-build/
cp ~/shared/cacert.pem ~/sites/sanctuary-blt-12/.ddev/web-build/

#echo "ARG BASE_IMAGE
#FROM $BASE_IMAGE
#ADD Cisco_Umbrella_Root_CA.crt /usr/local/share/ca-certificates/
#ADD cacert.pem /usr/local/share/ca-certificates/
#RUN update-ca-certificates --fresh
#RUN npm config set cafile /usr/local/share/ca-certificates/cacert.pem
#RUN export NODE_EXTRA_CA_CERTS=/usr/local/share/ca-certificates/cacert.pem
#
#RUN echo 'cafile=/usr/local/share/ca-certificates/Cisco_Umbrella_Root_CA.crt' | sudo tee -a ~/.npmrc
#RUN npm set strict-ssl false
#"  | sudo tee .ddev/web-build/Dockerfile > /dev/null

# sudo systemctl restart snapd
# sudo snap remove --purge firefox
# sudo snap install firefox
# sudo snap install chromium

# https://www.debugpoint.com/remove-snap-ubuntu/
snap list

sudo snap remove --purge firefox
sudo snap remove --purge snap-store
sudo snap remove --purge gnome-3-38-2004
sudo snap remove --purge gnome-42-2204
sudo snap remove --purge gtk-common-themes
sudo snap remove --purge snapd-desktop-integration
sudo snap remove --purge bare
sudo snap remove --purge core20
sudo snap remove --purge core22
sudo snap remove --purge snapd
sudo apt-get -yq remove --autoremove snapd

echo "
Package: snapd
Pin: release a=*
Pin-Priority: -10
" | sudo tee /etc/apt/preferences.d/nosnap.pref > /dev/null

sudo apt-get -yq update
sudo apt-get -yq install --install-suggests gnome-software
sudo add-apt-repository ppa:mozillateam/ppa
sudo apt-get -yq update
sudo apt-get -yq install -t 'o=LP-PPA-mozillateam' firefox
echo 'Unattended-Upgrade::Allowed-Origins:: "LP-PPA-mozillateam:${distro_codename}";' | sudo tee /etc/apt/apt.conf.d/51unattended-upgrades-firefox
echo "
Package: firefox*
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 501
" | sudo tee /etc/apt/preferences.d/mozillateamppa > /dev/null

# # Revert back to Snap in Ubuntu
# sudo rm /etc/apt/preferences.d/nosnap.pref
# sudo apt update && sudo apt upgrade
# sudo snap install snap-store
# sudo apt install firefox

# Applying certificate to Firefox Browser Open Firefox
# Go to: Firefox Settings > Privacy & Security > Certificates > View Certificate > Import > ~/shared/Cisco_Umbrella_Root_CA.cer > Trust this CA to identify web sites > OK
# Go to: Firefox Settings > Privacy & Security > Certificates > View Certificate > Import > (/usr/local/share/ca-certificates/mkcert_development_CA_XXXXXXXXXX) (~/.local/share/mkcert/rootCA.pem) > Trust this CA to identify web sites > OK

#https://ddev.readthedocs.io/en/latest/users/install/docker-installation/
# Add Docker's official GPG key:
sudo apt-get -yq update
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get -yq update
sudo apt-get -yq install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo docker run --name sudotest hello-world

# https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
docker run --name test hello-world
# Get the container id from the output of the doker ps command
docker ps -a
docker rm sudotest
docker rm test
docker ps -a

# https://docs.docker.com/engine/install/linux-postinstall/#configure-docker-to-start-on-boot-with-systemd
sudo systemctl enable docker.service
sudo systemctl enable containerd.service

# https://github.com/FiloSottile/mkcert#installation
# https://ddev.readthedocs.io/en/latest/users/install/ddev-installation/

# Add DDEV’s GPG key to your keyring
sudo sh -c 'echo ""'
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://pkg.ddev.com/apt/gpg.key | gpg --dearmor | sudo tee /etc/apt/keyrings/ddev.gpg > /dev/null
sudo chmod a+r /etc/apt/keyrings/ddev.gpg

# Add DDEV releases to your package repository
sudo sh -c 'echo ""'
echo "deb [signed-by=/etc/apt/keyrings/ddev.gpg] https://pkg.ddev.com/apt/ * *" | sudo tee /etc/apt/sources.list.d/ddev.list >/dev/null

# Update package information and install DDEV
sudo sh -c 'echo ""'
sudo apt update && sudo apt install -y ddev

ddev debug test
mkdir ddevtest
cd ddevtest
ddev config
ddev debug test
ddev debug testcleanup
ddev remove --remove-data --omit-snapshot
ddev delete -Oy tryddevproject-6354

read -p "Install 1Password? (y/N): " -n 1 -r
if [[ $1PASSWORD =~ ^[Yy]$ ]]
then
  echo "you chose to install 1Password"
  export INSTALL_1PASSWORD=true
else
  echo "you chose to skip 1Password"
  export INSTALL_1PASSWORD=false
fi

if [[ ${INSTALL_1PASSWORD} == true ]]; then
  # 1Password
  curl -sS https://downloads.1password.com/linux/keys/1password.asc | sudo gpg --dearmor --output /usr/share/keyrings/1password-archive-keyring.gpg
  echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/1password-archive-keyring.gpg] https://downloads.1password.com/linux/debian/amd64 stable main' | sudo tee /etc/apt/sources.list.d/1password.list
  sudo mkdir -p /etc/debsig/policies/AC2D62742012EA22/
  curl -sS https://downloads.1password.com/linux/debian/debsig/1password.pol | sudo tee /etc/debsig/policies/AC2D62742012EA22/1password.pol
  sudo mkdir -p /usr/share/debsig/keyrings/AC2D62742012EA22
  curl -sS https://downloads.1password.com/linux/keys/1password.asc | sudo gpg --dearmor --output /usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg
  sudo apt-get -yq update && sudo apt install 1password

  # Go to: https://1password.com/downloads/browser-extension/

  # # Remove 1Password
  # sudo apt remove --autoremove 1password
  # sudo rm /etc/apt/sources.list.d/1password.list
fi

sudo apt-get -yq autoremove
