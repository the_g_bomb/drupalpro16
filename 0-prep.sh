#!/bin/bash

#======================================| Import Variables
# Make sure you have edited this file
# CWD based on http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

echo "Starting installation, please answer n to anything you are unsure of."

echo ""
echo "This script can stop ubuntu asking you for a password everytime you run sudo."
read -p "Bypass Sudo password requirement on Ubuntu? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
  echo ""
  echo "you chose to bypass sudo passwords"
  export OVERRIDE_UBUNTU_SECURITY=true
else
  echo ""
  echo "you chose to leave sudo as it was installed"
  export OVERRIDE_UBUNTU_SECURITY=false
fi
if [[ ${OVERRIDE_UBUNTU_SECURITY} == true ]]; then
  #======================================| The last password you'll ever need.
  # add current user to sudoers file - careful, this line could brick the box.
  clear
  echo "WARNING: THIS WILL OVERRIDE DEFAULT UBUNTU SECURITY. IF YOU DON'T WANT TO DO THIS, PRESS CTRL-C AND SKIP THIS SCRIPT.
  Otherwise..."
  echo "${USER} ALL=(ALL) NOPASSWD: ALL" | sudo tee -a "/etc/sudoers.d/${USER}" > /dev/null
  sudo chmod 0440 "/etc/sudoers.d/${USER}"

  # Add current user to root 'group' to make it easier to edit config files
  # note: seems unsafe for anyone unaware.
  sudo adduser $USER root
fi

echo ""
echo "Complete. Please run:"
echo "$ sudo reboot now"
echo "When restarted, please continue with:"
echo "$ ./1-update.sh"

stage_finished=0
exit "$stage_finished"
