#!/bin/bash
set -e

#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

echo "Running, please wait..."

## Ubuntu bugfix https://bugs.launchpad.net/ubuntu/+source/gdk-pixbuf/+bug/1282294
#sudo apt-get ${APTGET_VERBOSE} install libgdk-pixbuf2.0-dev

## Upgrade
sudo apt-get ${APTGET_VERBOSE} update
echo "Updating, please wait..."
sudo apt-get ${APTGET_VERBOSE} upgrade
echo "Cleaning up, please wait..."
sudo apt-get ${APTGET_VERBOSE} autoremove && sudo apt-get ${APTGET_VERBOSE} clean all && sudo apt-get ${APTGET_VERBOSE} autoclean all

# With the installation of PHP apache may now be set to start automatically which causes issues with DDEVs ports
echo "Installing certain PHP modules also installs libapache2-mod-php8.1 which starts apache."
echo "Stopping apache2 ..."
sudo service apache2 stop
sudo systemctl disable apache2
sudo update-rc.d -f apache2 remove

echo ""
echo "Complete. Please run:"
echo "$ ./2a-shared_drives.sh"

stage_finished=0
exit "$stage_finished"
