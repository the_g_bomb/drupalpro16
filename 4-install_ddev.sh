#!/bin/bash

#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

echo "Running some initial installations, please wait..."

#======================================| Install DDev packages

sudo apt-get ${APTGET_VERBOSE} update
sudo apt-get ${APTGET_VERBOSE} install libnss3-tools mkcert

echo ""
read -p "This script needs to have certificates correct to find the appropriate keys, Do you want to install them now? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
  echo ""
  echo "Please run:"
  echo "$ 2c-install_certificates.sh"
  exit
fi
echo ""
echo "DDEV attempts to launch Firefox as a part of the installation script, which fails if Firefox has been installed with snap. "
read -p "Do you want to halt the installation and run '2c-remove_snap.sh' instead? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
  echo ""
  echo "Please run:"
  echo "$ ./2c-remove_snap.sh"
  exit
fi

# https://ddev.readthedocs.io/en/latest/users/install/ddev-installation/
# https://github.com/FiloSottile/mkcert#installation

# Add DDEV’s GPG key to your keyring
sudo sh -c 'echo ""'
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://pkg.ddev.com/apt/gpg.key | gpg --dearmor | sudo tee /etc/apt/keyrings/ddev.gpg > /dev/null
sudo chmod a+r /etc/apt/keyrings/ddev.gpg

# Add DDEV releases to your package repository
echo ""
sudo sh -c 'echo ""'
echo "deb [signed-by=/etc/apt/keyrings/ddev.gpg] https://pkg.ddev.com/apt/ * *" | sudo tee /etc/apt/sources.list.d/ddev.list >/dev/null

# Update package information and install DDEV
echo ""
echo "Installing DDEV."
sudo sh -c 'echo ""'
sudo apt-get ${APTGET_VERBOSE} update
sudo apt-get ${APTGET_VERBOSE} install ddev

echo "Testing DDEV."
mkdir -p ddevtest
cd ddevtest
ddev config
ddev debug test
ddev remove --remove-data --omit-snapshot
# ddev delete -Oy tryddevproject-6354
cd ..
rm -rf ddevtest
cd tryddevproject-*
ddev remove --remove-data --omit-snapshot
cd ..
rm -rf tryddevproject-*

#echo "ARG BASE_IMAGE
#FROM \$BASE_IMAGE
#ADD Cisco_Umbrella_Root_CA.cer /usr/local/share/ca-certificates/
#ADD cacert.pem /usr/local/share/ca-certificates/
#RUN update-ca-certificates --fresh
#RUN npm config set cafile /usr/local/share/ca-certificates/cacert.pem
#RUN export NODE_EXTRA_CA_CERTS=/usr/local/share/ca-certificates/cacert.pem
#RUN echo 'cafile=/usr/local/share/ca-certificates/cacert.crt' | sudo tee -a ~/.npmrc
#
#RUN npm set strict-ssl false
#"  | sudo tee .ddev/web-build/Dockerfile > /dev/null

echo ""
echo "Complete."
echo "This is a work in progress. Please help out where you can."
echo "Incomplete and not fully tested, but to install IDEs, please run:"
echo "$ ./5-ides.sh"

stage_finished=0
exit "$stage_finished"
