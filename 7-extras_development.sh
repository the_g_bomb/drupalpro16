#!/bin/bash
set -e

#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

cd ~

#======================================| Composer
echo ""
echo "This script installs composer outside of DDEV, this is needed if you run git-hooks created by BLT."
read -p "Do you want to install composer and the required files now? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
#if [[ ${INSTALL_DRUSH} == true ]]; then

  echo ""
  echo "Running some initial installations, please wait..."
  sudo apt-get update
  sudo apt-get ${APTGET_VERBOSE} install php php-bcmath php-cli php-common php-curl php-gd php-mbstring php-sqlite3 php-xml
  sudo apt install software-properties-common
  sudo LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php
  sudo apt-get update
  # cli needed for git hooks
  # curl needed for acli
  # mbstring needed for grumphp
  # gd needed for drupal11
  sudo apt-get ${APTGET_VERBOSE} install php8.1 php8.1-cli php8.1-curl php8.1-gd php8.1-mbstring

  # Install composer Globally
  # FROM: https://getcomposer.org/doc/faqs/how-to-install-composer-programmatically.md

  EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig)
  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
  ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

  if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
  then
      >&2 echo 'ERROR: Invalid installer signature'
      rm composer-setup.php
      exit 1
  fi
  php composer-setup.php --quiet
  rm composer-setup.php

  sudo mv composer.phar /usr/local/bin/composer
  sudo chmod -R u=rwX,go=rX /usr/local/bin

  echo '
  export PATH="${HOME}/.config/composer/vendor/bin:$PATH"
' | sudo tee -a $HOME/.bashrc

  cd ~
  /usr/local/bin/composer -v

  # With the installation of PHP apache may now be set to start automatically which causes issues with DDEVs ports
  echo "Installing certain PHP modules also installs libapache2-mod-php8.1 which starts apache."
  echo "Stopping apache2 ..."
  sudo service apache2 stop
  sudo systemctl disable apache2
  sudo update-rc.d -f apache2 remove

fi

#======================================| Oh my Zsh
echo ""
read -p "Do you want to install oh-my-zsh and the required files now? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then

  echo ""
  echo "Running some initial installations, please wait..."
  cd ~
  sudo apt-get update
  sudo apt-get ${APTGET_VERBOSE} install build-essential curl wget file git
  sudo apt-get ${APTGET_VERBOSE} install zsh
  zsh --version

  sh -c "$(wget https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

echo "Installation complete"
  echo "You may want to edit the file ~/.zshrc to enable any plugins"
  echo "e.g.:"
  echo "  plugins=(colored-man-pages dirhistory git history zsh-syntax-highlighting zsh-autosuggestions)"
  echo ""
  echo "If you need to remove oh-my-zsh, please run:"
  echo "$ sudo uninstall oh_my_zsh"
  echo "$ sudo apt --purge remove zsh"
  echo "$ chsh -s $(which bash)"

fi


stage_finished=0
exit "$stage_finished"
