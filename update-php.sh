#!/bin/bash
set -e

#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

if [[ "${APTGET_VERBOSE}" == ""* ]]; then
  APTGET_VERBOSE=-y
fi

echo "This script will update PHP beyond the version shipped with Ubuntu..."

#======================================| PHP
echo ""
read -p "Do you need to update PHP? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
  echo "Adding new repository and installing PHP, please wait ..."
  sudo apt install software-properties-common
  sudo LC_ALL=C.UTF-8 add-apt-repository ${APTGET_VERBOSE} ppa:ondrej/php
  sudo apt-get ${APTGET_VERBOSE} update
  sudo apt-get ${APTGET_VERBOSE} install php8.1 php8.1-cli
  # sudo update-alternatives --set php /usr/bin/php8.2

  ## Upgrade
  echo "Updating, please wait..."
  sudo apt-get ${APTGET_VERBOSE} upgrade
  echo "Cleaning up, please wait..."
  sudo apt-get ${APTGET_VERBOSE} autoremove && sudo apt-get ${APTGET_VERBOSE} clean all && sudo apt-get ${APTGET_VERBOSE} autoclean all

  # With the installation of PHP apache may now be set to start automatically which causes issues with DDEVs ports
  echo "Installing certain PHP modules also installs libapache2-mod-php8.1 which starts apache."
  echo "Stopping apache2 ..."
  sudo service apache2 stop
  sudo systemctl disable apache2
  sudo update-rc.d -f apache2 remove

fi
echo ""
echo "Complete."
echo "This is a work in progress. Please help out where you can."
echo "Incomplete and not fully tested."

stage_finished=0
exit "$stage_finished"
