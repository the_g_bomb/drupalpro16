#!/bin/bash

#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

echo "Running some initial installations, please wait..."

#======================================| Install Docker packages
# Define package names, and debconf config values.  Keep package names in sync.

# https://github.com/FiloSottile/mkcert#installation
sudo apt-get ${APTGET_VERBOSE} update
sudo apt-get ${APTGET_VERBOSE} install build-essential ca-certificates curl file git gnupg libnss3-tools mkcert procps wget

echo ""
read -p "Do you need to install the Cisco Umbrella certificates? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
  sudo curl -L https://static.caci.co.uk/umbrella/Cisco_Umbrella_Root_CA.cer -o /usr/local/share/ca-certificates/Cisco_Umbrella_Root_CA.crt
  # sudo cp /home/${USER}/shared/Cisco_Umbrella_Root_CA.cer /usr/local/share/ca-certificates/Cisco_Umbrella_Root_CA.crt
  # sudo cp /home/${USER}/shared/cacert.pem /usr/local/share/ca-certificates/
  #sudo cp /home/${USER}/shared/ca-bundle.crt /usr/local/share/ca-certificates/
  #sudo cp /home/${USER}/shared/cacert.crt /usr/local/share/ca-certificates/
  #sudo cp /home/${USER}/shared/cert.pem /usr/local/share/ca-certificates/
fi

mkcert -install
sudo update-ca-certificates

echo ""
echo "Complete. Please run:"
echo "$ ./2d-remove_snap.sh"

stage_finished=0
exit "$stage_finished"
