#!/bin/bash

#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

echo "Running some initial installations, please wait..."

#======================================| Install Docker packages
# Define package names, and debconf config values.  Keep package names in sync.

sudo apt-get ${APTGET_VERBOSE} update
sudo apt-get ${APTGET_VERBOSE} install build-essential ca-certificates curl file git gnupg libnss3-tools mkcert procps wget

echo ""
read -p "This script needs to have certificates correct to find the appropriate keys, Do you want to install them now? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
  echo ""
  echo "Please run:"
  echo "$ 2b-install_certificates.sh"
  exit
fi
echo ""
echo "DDEV attempts to launch Firefox as a part of the installation script, which fails if Firefox has been installed with snap."
read -p "Do you want to halt the installation and run '2c-remove_snap.sh' instead? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
  echo ""
  echo "Please run:"
  echo "$ 2c-remove_snap.sh"
  exit
fi

echo ""
echo "Setting up Docker"
# Set up Docker's apt repository.
# https://ddev.readthedocs.io/en/latest/users/install/docker-installation/
# https://www.powercms.co.uk/article/ddev-installation-ubuntu

# Add Docker's official GPG key:
sudo apt-get ${APTGET_VERBOSE} install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install the Docker packages.
sudo apt-get ${APTGET_VERBOSE} update
sudo apt-get ${APTGET_VERBOSE} install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
#echo ""
#echo "To test docker with sudo use the following commands;"
#echo "  $ docker run --name sudotest hello-world"
## Get the container id from the output of the docker ps command
#echo "  $ docker ps -a"
#echo "  $ docker rm sudotest"
#echo "  $ docker ps -a"

# https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user
# https://www.geeksforgeeks.org/groupadd-command-in-linux-with-examples/
# Create the docker group.
echo ""
echo "Create docker group."
[ $(getent group docker) ] || sudo groupadd docker

# Add your user to the docker group.
echo "Adding $USER to docker group."
sudo usermod -aG docker $USER
#sudo adduser $USER docker

# Activate the changes to groups.
#echo "Activate the changes to groups"
#sudo newgrp docker
#su - ${USER}
#groups

# https://docs.docker.com/engine/install/linux-postinstall/#configure-docker-to-start-on-boot-with-systemd
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
#sudo systemctl status docker
#sudo service docker start

echo ""
echo "To test docker without sudo use the following commands;"
echo "  $ docker run --name test hello-world"
# Get the container id from the output of the docker ps command
echo "  $ docker ps -a"
echo "  $ docker rm test"
echo "  $ docker ps -a"

echo ""
echo "You may experience permission issues when attempting this. In which case, please run:"
echo "$ sudo reboot now"
echo "When restarted, please continue with:"
echo "$ ./3b-test_docker.sh"

stage_finished=0
exit "$stage_finished"
