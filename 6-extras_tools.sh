#!/bin/bash
set -e

#======================================| Theming Tools & Desktop Utilities
#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

APP_FOLDER='/usr/share'
#======================================| PART1
#======================================| Setup PPA's and install
echo "Attempting to install ardesia unity-lens-graphicdesign unity-lens-utilities unity-lens-wikipedia"
read -p "Do you need to install some Graphical Extras? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
#if [[ ${INSTALL_GRAPHIC_XTRAS} == true ]]; then
  GRAPHIC_PKS="ardesia unity-lens-graphicdesign unity-lens-utilities unity-lens-wikipedia"
  #======================================| Add Unity Scopes PPA
  echo 'deb http://ppa.launchpad.net/scopes-packagers/ppa/ubuntu xenial main ' | sudo tee -a /etc/apt/sources.list.d/scopes-packagers-xenial.list > /dev/null
  echo 'deb-src http://ppa.launchpad.net/scopes-packagers/ppa/ubuntu xenial main ' | sudo tee -a /etc/apt/sources.list.d/scopes-packagers-xenial.list > /dev/null
  sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 48894010
  UPDATE_APT=1
fi

echo ""
echo "Attempting to install synaptic bleachbit diodon diodon-plugins autokey-gtk"
read -p "Do you need to install some Power Utilities? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
#if [[ ${INSTALL_POWER_UTILS} == true ]]; then
  INSTALL_POWER_UTILS=true
  # power utils
  PWR_UTLS_PKS="synaptic bleachbit diodon diodon-plugins autokey-gtk"
  #======================================| Add Diodon Clipboard Manager PPA
  echo 'deb http://ppa.launchpad.net/diodon-team/stable/ubuntu xenial main' | sudo tee -a /etc/apt/sources.list.d/diodon-xenial.list > /dev/null
  echo 'deb-src http://ppa.launchpad.net/diodon-team/stable/ubuntu xenial main' | sudo tee -a /etc/apt/sources.list.d/diodon-xenial.list > /dev/null
  sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 523884B2
  UPDATE_APT=1
fi

echo ""
echo "Attempting to install guake nautilus-open-terminal grsync"
read -p "Do you need to install some Terminal Utilities? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
#if [[ ${INSTALL_TERMINAL_UTILS} == true ]]; then
  INSTALL_TERMINAL_UTILS=true
  TERMINAL_PKS="guake nautilus-open-terminal grsync"
fi

echo ""
echo "Attempting to install gitg meld git-gui gitk nautilus-compare"
read -p "Do you need to install some Git Power tools? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
#if [[ ${INSTALL_GIT_POWER} == true ]]; then
  INSTALL_GIT_POWER=true
  #======================================| Add GIT tools
  GIT_PKS="gitg meld git-gui gitk nautilus-compare"
fi

#======================================| GITKRAKEN
echo ""
read -p "Do you need to install the GitKraken and tools? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
echo "Downloading GitKraken, please wait ..."
#  sudo wget -O "/opt/gitkraken-amd64.deb https://release.gitkraken.com/linux/gitkraken-amd64.deb
#  sudo apt install /opt/gitkraken-amd64.deb
  GITKRAKEN_ROOT=gitkraken
  GITKRAKEN_URL=https://release.gitkraken.com/linux/gitkraken-amd64.tar.gz
  sudo mkdir -p "${APP_FOLDER}/${GITKRAKEN_ROOT}"
  sudo wget ${WGET_VERBOSE} -O "${APP_FOLDER}/gitkraken.tar.gz" --referer="${REFERER}" --user-agent="${USERAGENT}" --header="${HEAD1}" --header="${HEAD2}" --header="${HEAD3}" --header="${HEAD4}" --header="${HEAD5}" "${GITKRAKEN_URL}"
  sudo tar xfz "${APP_FOLDER}/gitkraken.tar.gz" -C "${APP_FOLDER}/${GITKRAKEN_ROOT}" --strip-components 1 && sudo rm "${APP_FOLDER}/gitkraken.tar.gz"
  cd "${APP_FOLDER}/${GITKRAKEN_ROOT}"
  if [ -e ${APP_FOLDER}/${GITKRAKEN_ROOT}/gitkraken.png ]; then sudo cp ${APP_FOLDER}/gitkraken/gitkraken.png /usr/share/pixmaps/gitkraken.png; fi
  sudo chmod +x ./gitkraken
  sudo ln -s ${APP_FOLDER}/${GITKRAKEN_ROOT}/resources/bin/gitkraken.sh /usr/bin/gitkraken

  sudo cat > "/usr/share/applications/gitkraken.desktop" <<END
[Desktop Entry]
Name=GitKraken
Comment=Unleash your repo
GenericName=Git Client
Exec=/usr/share/gitkraken/gitkraken %U
Icon=/usr/share/pixmaps/gitkraken.png
Type=Application
StartupNotify=true
Categories=GNOME;GTK;Development;RevisionControl;
MimeType=text/plain;
StartupWMClass=gitkraken
END

sudo chmod 750 "/usr/share/applications/gitkraken.desktop"

  sudo cat > "/usr/share/applications/gitkraken-url-handler.desktop" <<END
[Desktop Entry]
Name=GitKraken
Comment=Unleash your repo
GenericName=Git Client
Exec=/usr/bin/gitkraken --uri=%U
Icon=/usr/share/pixmaps/gitkraken.png
Type=Application
NoDisplay=true
StartupNotify=true
Categories=GNOME;GTK;Development;RevisionControl;
MimeType=x-scheme-handler/gitkraken;
StartupWMClass=gitkraken
END

sudo chmod 750 "/usr/share/applications/gitkraken-url-handler.desktop"

fi

#======================================| INSTALL EXTRA INDICATORS
echo ""
echo "Attempting to configure some Unity Indicators"
read -p "Do you need to configure Extra Indicators? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
#if [ "${INSTALL_EXTRA_INDICATORS}" == true ]; then
  INSTALL_EXTRA_INDICATORS=true
  unset new_indicators
  sudo apt-add-repository -y ppa:alanbell/unity && new_indicators+="unity-window-quicklists"" "
  sudo apt-add-repository -y ppa:bhdouglass/indicator-remindor && new_indicators+="indicator-remindor"" "
  sudo add-apt-repository -y ppa:indicator-multiload/stable-daily && new_indicators+="indicator-multiload"" "
  UPDATE_APT=1
fi

echo ""
echo "Attempting to install the Gimp graphical editor"
read -p "Do you need to install Gimp? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
#if [[ ${INSTALL_GIMP} == true ]]; then
  # Install graphics editors - weights about 25mb
  GIMP_PKS="gimp gimp-data gimp-extras icc-profiles-free" #  @TODO: suggest to user of non-free icc profiles
  # sudo add-apt-repository ppa:otto-kesselgulasch/gimp
  # setup gimp ppa
  echo 'deb http://ppa.launchpad.net/otto-kesselgulasch/gimp/ubuntu xenial main' | sudo tee -a /etc/apt/sources.list.d/otto-kesselgulasch-gimp-xenial.list > /dev/null
  echo 'deb-src http://ppa.launchpad.net/otto-kesselgulasch/gimp/ubuntu xenial main' | sudo tee -a /etc/apt/sources.list.d/otto-kesselgulasch-gimp-xenial.list > /dev/null
  sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 614C4B38
  UPDATE_APT=1
fi

echo ""
echo "Attempting to install the Inkscape vector graphic tool"
read -p "Do you need to install Inkscape? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
#if [[ ${INSTALL_INKSCAPE} == true ]]; then
  INKSCAPE_PKS="inkscape"
fi

echo ""
echo "Attempting to install Compass and Sass"
read -p "Do you need to install Compass and Sass? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
#if [[ ${INSTALL_COMPASS} == true ]]; then
  # Install compass (which needs ruby)
  sudo apt-get ${APTGET_VERBOSE} install ruby-full
  sudo gem install sass
  sudo gem install compass
fi

# Install chrome browser (Webkit - fork of KHTML/Konquerer, also used by Safari)
# sudo apt-get ${APTGET_VERBOSE} install chromium-browser
# sudo ln -s /usr/lib/flashplugin-installer/libflashplayer.so /usr/lib/chromium-browser/plugins/

if [[ ${UPDATE_APT} -gt 0 ]]; then
  sudo apt-get update
fi

#======================================| Install packages
sudo apt-get ${APTGET_VERBOSE} install gnome-activity-journal p7zip gnote &
wait
sudo apt-get ${APTGET_VERBOSE} install ${GRAPHIC_PKS} ${PWR_UTLS_PKS} ${TERMINAL_PKS} ${GIT_PKS} ${new_indicators} ${FLASH_PKS} ${GIMP_PKS} ${INKSCAPE_PKS}

echo ""
echo "Installation Complete. Now running configuration changes..."

#======================================| PART2
#======================================| Add some nice Configurations

if [[ ${INSTALL_POWER_UTILS} == true ]]; then
  #======================================| Diodon clipboard and Autokey automation
  # Whitelist autokey for Unity panel
  if grep -iq 'autokey-gtk' <(echo `gsettings get com.canonical.Unity.Panel systray-whitelist`); then
    echo "'Autokey' already exists in the Unity panel whitelist. Nothing to do here.";
  else echo "Adding 'Autokey' to Unity panel whitelist." && gsettings set com.canonical.Unity.Panel systray-whitelist "`echo \`gsettings get com.canonical.Unity.Panel systray-whitelist | tr -d ]\`,\'autokey-gtk\']`";
  fi

  #======================================| Xchat IRC
  # Whitelist xchat for Unity panel
  if grep -iq 'xchat' <(echo `gsettings get com.canonical.Unity.Panel systray-whitelist`); then
    echo "'xchat' already exists in the Unity panel whitelist. Nothing to do here.";
  else echo "Adding 'xchat' to Unity panel whitelist." && gsettings set com.canonical.Unity.Panel systray-whitelist "`echo \`gsettings get com.canonical.Unity.Panel systray-whitelist | tr -d ]\`,\'xchat\']`"; fi
fi
if [[ ${INSTALL_TERMINAL_UTILS} == true ]]; then
  #Change Defaults for guake
  gconftool -s /apps/guake/keybindings/global/show_hide --type=string "F4"      # Change to F4 since F12 means firebug/dev utilities in most browsers
  gconftool -s /apps/guake/general/history_size --type=int 8192                 # more history
  gconftool -s /apps/guake/style/background/transparency --type=int 10          # Easier to see
  gconftool -s /apps/guake/general/window_ontop --type=bool false               # Alow dialog pop-ups to take focus
  gconftool -s /apps/guake/style/font/style --type=string "Monospace 13"        # Easier to see
fi
if [[ ${INSTALL_GIT_POWER} == true ]]; then
  #======================================| configure GIT Tools
  # mostly based off http://cheat.errtheblog.com/s/git
  git config --global alias.st status
  git config --global alias.ci commit
  git config --global alias.br branch
  git config --global alias.co checkout
  git config --global alias.df diff
  git config --global alias.lg "log --graph --pretty=format:'%C(blue)%h %Creset%C(reverse bold blue)%d%Creset %C(white)%s %C(green bold)%cr%Creset %C(green)%aN' --abbrev-commit --decorate"
  git config --global alias.clear '!git add -A && git reset --hard'
  git config --global alias.unstage "reset HEAD --"
  git config --global alias.ign "ls-files -o -i --exclude-standard"
  git config --global alias.gitkconflict '!gitk --left-right HEAD...MERGE_HEAD'
  git config --global alias.alias "config --get-regexp alias"
  git config --global apply.whitespace error-all
  git config --global color.ui auto
  git config --global color.diff.whitespace "red reverse"
  git config --global color.diff.meta "magenta"
  git config --global color.diff.frag "yellow"
  git config --global color.diff.old "red"
  git config --global color.diff.new "green bold"
  git config --global color.grep.context yellow
  git config --global color.grep.filename blue
  git config --global color.grep.function "yellow bold"
  git config --global color.grep.linenumber "cyan bold"
  git config --global color.grep.match red bold
  git config --global color.grep.selected white
  git config --global color.grep.separator blue
  git config --global color.status.added yellow
  git config --global color.status.changed red
  git config --global color.status.untracked "cyan bold"
  git config --global diff.tool meld
  git config --global gui.editor geany
  git config --global merge.summary true
  git config --global merge.tool meld
fi

#======================================| INSTALL EXTRA INDICATORS
if [ "${INSTALL_EXTRA_INDICATORS}" == true ]; then
  if [ -f "/etc/xdg/autostart/unity-window-quicklists.desktop" ]; then # fix autostart bug if window quicklists is installed.  won't harm anything if ppa is already updated.
    sudo sed -i 's/OnlyShowIn=UNITY/OnlyShowIn=Unity/g' /etc/xdg/autostart/unity-window-quicklists.desktop
  fi
  #show all autostart applications
  sudo sed -i 's/NoDisplay=true/NoDisplay=false/g' /etc/xdg/autostart/*.desktop
fi

echo ""
echo "Attempting to install extra browsers."
read -p "Do you need to install Additional browsers? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""

  # Install tor browser
  sudo apt install tor torbrowser-launcher
fi
#
# Install chrome browser (Webkit - fork of KHTML/Konquerer, also used by Safari)
# sudo apt-get ${APTGET_VERBOSE} install chromium-browser
# sudo ln -s /usr/lib/flashplugin-installer/libflashplayer.so /usr/lib/chromium-browser/plugins/

echo ""
echo "Complete."
echo "This is a work in progress. Please help out where you can."
echo "Incomplete and not fully tested, but to install some extra dev tools, please run:"
echo "$ ./7-extras_development.sh"

stage_finished=0
exit "$stage_finished"
