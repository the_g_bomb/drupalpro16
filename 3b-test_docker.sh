#!/bin/bash

#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

#======================================| Install Docker packages
# Define package names, and debconf config values.  Keep package names in sync.

echo ""
echo "Testing Docker"
docker run --name test hello-world
docker ps -a
docker rm test
docker ps -a

echo ""
echo "Complete. Please run:"
echo "$ ./4-install_ddev.sh"

stage_finished=0
exit "$stage_finished"
