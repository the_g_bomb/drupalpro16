DrupalPro16
===========

This is a collection of shell scripts that can be run in a clean installation of Ubuntu 16.04 (64bits) desktop edition in order to build a Drupal development environment similar to the pre-configured development environment that was offered for download as a VirtualBox image for Ubuntu 12.04 (32bit).

This work is derived from the QuickStart and DrupalPro projects (see credits below) and also I want to give a nod to [jcmartinez](https://www.drupal.org/u/jcmartinez) for the work he has done in upgrading some scripts to 14.04, which helped my approach in places.

This code has been tested in a VirtualBox and in VMWare machines and could work in a physical machine after applying some security hardening to the system.

Tested on:

-[x] Ubuntu 22.04 (64bit)

Note: For Lubuntu and Linux Mint use the selective installation method explained below and skip the cleanup 1-cleanup.sh script. 

# How to use

## New instructions for CACI Installation

### Download Ubuntu Desktop ISO from:
Go to: https://ubuntu.com/download/desktop  
Grab the file: https://ubuntu.com/download/desktop/thank-you?version=22.04.3&architecture=amd64  
e.g: https://releases.ubuntu.com/22.04.3/ubuntu-22.04.3-desktop-amd64.iso?_ga=2.222921241.342556869.1705069738-1636726850.1704206951

#### Other flavours
These scripts should work with other flavours, although they haven't been tested, so your experience may vary, feel free to try out:
- [kubuntu](https://kubuntu.org/getkubuntu/)
- [lubuntu](https://lubuntu.me/downloads/)
- [xubuntu](https://xubuntu.org/download/)

### Get CA Certificates to appease Cisco Umbrella:
Cisco Umbrella CA certificate
- https://static.caci.co.uk/umbrella/Cisco_Umbrella_Root_CA.cer
Pre-made bundle (based of the Mozilla one as above)
- https://static.caci.co.uk/umbrella/ca-bundle.crt
- https://static.caci.co.uk/umbrella/cacert.crt
- https://static.caci.co.uk/umbrella/cacert.pem
- https://static.caci.co.uk/umbrella/cert.pem

Check you are on the developer network
```
gpresult -r -v | Select-String -Pattern "AnyConnect"
AnyConnect_Dev
```

### Create a VM

Create a New Environment with:
 
16384 MB Base Memory
2 Processors
25GB Hard Drive

Get to the end and Uncheck the box:
Power on this virtual machine after creation

Go to: VM > Settings
Switch to the Options tab
Select Shared Folders

Checked Always enabled

Add shared folders as require from your Windows machine and name them. Some suggested shares are:
- `C:/Users/${USER}/.aws` - aws
- `C:/Users/${USER}/.acquia` - acquia
- `C:/Users/${USER}/shared` - shared
- `C:/Users/${USER}/.ssh` - ssh_shared

With those added, it is time to power on the new VM and install Ubuntu:

### Power on the new VM

Skip unattended Installation ticked

Try or Install Ubuntu
- Install Ubuntu
- Choose Keyboard
- Minimal Installation
- Install 3rd Party software
- Choose Timezone

Mount the shared drive to access the scripts and certificates

```
echo ".host:/shared /home/${USER}/shared/ fuse.vmhgfs-fuse defaults,allow_other,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
```
Reboot

### Install the certificates
```
sudo cp /home/${USER}/shared/Cisco_Umbrella_Root_CA.cer /usr/local/share/ca-certificates/Cisco_Umbrella_Root_CA.crt
sudo update-ca-certificates
```
Now install git and grab this set of scripts
```
sudo apt-get install git
git clone git@bitbucket.com:the_g_bomb/drupalpro16.git ~/drupalpro
```

## Install Ubuntu

Install Ubuntu 22.04 (64bits) Desktop Edition on your computer either in VirtualBox or in VMWare.

IMPORTANT: During the installation you will be asked to create a user account. It is recommended that the username for this account is: drupalpro but it can anything.

Once the installation is done you can login to your new system using your "drupalpro" account and the password that you selected.

Note: At this point if you are installing Ubuntu in a VirtualBox machine you may want to install also the VirtualBox guest additions. Instructions at https://www.virtualbox.org/manual/ch04.html (look under "Installing the Linux Guest Additions"). If you are installing in a physical machine ignore this note. 

## Update Ubuntu for the first time

Open the terminal by pressing Ctrl+Alt+t and type the following two commands:

    sudo apt-get update
    sudo apt-get upgrade

## Retrieve this code using Git

### Install Git

In the terminal type:

    sudo apt-get install git

### Retrieve this code

In the terminal type:

    git clone git@bitbucket.com:the_g_bomb/drupalpro16.git ~/drupalpro

## Run the configuration scripts

In the terminal navigate to the directory where you just cloned the installation scripts:

    cd ~/drupalpro

### The code from here on needs more testing and the installation methods have changed considerably.

There are two ways of running the scripts that will set Drupalpro for you (use only one):

A) The fast way - If you want to run all the scripts at once use the run_all.sh script as follows:

    bash run_all.sh

B) The selective way - Run the configuration scripts one-by-one. A good reason for doing so is if you would like to keep the office software that comes with Ubuntu; in this case you would run the scripts as explained below with the exception of 1-cleanup.sh because this script will erase the office package, games, etc. Aother reason could be that you prefer installing a diffrent IDE and don't want to install Netbeans, in this case you don't need to run the script 4-install-betbeans.sh. In case you want to be selective do as follows:

    ./0-prep.sh
    ./1-update.sh
    ./2a-shared_drives.sh
    ./2b-add_ssh_keys.sh
    ./2c-install_certificates.sh
    ./2d-remove_snap.sh
    ./3a-install_docker.sh
    ./3b-test_docker.sh
    ./3b-test_docker.sh
    ./4-install_ddev.sh
    ./5-ides.sh [WIP]
    ./6-extras_tools.sh [WIP]
    ./7-extras_development.sh [WIP]

## Create your first Drupal site

In your terminal navigate to the "websites" directory:

    cd ~/sites

... and run the following command to install a Website:

    drush qc all --domain=d7.dev --codepath=/home/drupalpro/websites/d7.dev --makefile=/home/drupalpro/make_templates/d7.make --profile=standard

Now you should be able to visit your website using a browser and typing http://d7.dev in the address bar.

If you want to install another site use the same command above and modify the variables --domain and --codepath.

If you want to destroy a site run the following:

    drush qd --domain=d7.dev

# Possible issues

## Drush installation may fail

In some cases the installation of Drush fails. You can test whether Drush is installed by running the command:

    drush --version

If the above can't detect the version of Drush it is probably because the script 5-install-drush.sh failed to complete around the following lines:

      FILE = $HOME/.bashrc
      if [ -f $FILE ];
      then
         sed -i '1i export PATH="$HOME/.composer/vendor/bin:$PATH"' $HOME/.bashrc
      else
         echo 'export PATH="$HOME/.composer/vendor/bin:$PATH"' >> $HOME/.bashrc
      fi

Suggestions to fix this in the script are welcome. In the meantime to fix the problem you need to determine whether the file `~/.bashrc` was created and then run one of the two commands above accordingly. After that you can run `drush --version` again to see if Drush got installed.

Last but not least, once Drush gets installed you should run `drush cc drush` so that it recognizes the addon scripts.

## Apache may give you a access/permission error

If after adding a new site using Drush you get an access denied error while visiting the URL of the new site it may be that Apache don't have the permissions required to execute files. Since permissions are inherited you need to ensure that the directries above your webroot are no restricting apache. In cases when this issue has arised the problem has been solved by giving 755 permission to the drupalpro directory as follows:

    cd /home
    sudo chmod 755 -R drupalpro 

# ToDos

So far only one IDE environment (Netbeans) has been installed. If you know how to script the installation of other environments please fork this code and it will be added.

# Credits

DrupalPro14 is based on the Quickstart project (https://www.drupal.org/project/quickstart) and the DrupalPro project (https://www.drupal.org/project/drupalpro), the first by Michael Cole and the latter by Mike Stewart.
  - [michaelcole](https://www.drupal.org/u/michaelcole)
  - [mike-stewart](https://www.drupal.org/u/mike-stewart)
