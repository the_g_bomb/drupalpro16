#!/bin/bash
set -e

#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

echo "This script will update PHPStorm..."

PHPSTORM_URL_OLD="https://download.jetbrains.com/webide/PhpStorm-2023.3.6.tar.gz"
PHPSTORM_ROOT_OLD="PhpStorm-2023.3.6"
PHPSTORM_URL_NEW="https://download.jetbrains.com/webide/PhpStorm-2024.1.1.tar.gz"
PHPSTORM_ROOT_NEW="PhpStorm-2024.1.1"

PHPSTORM_URL=${PHPSTORM_URL_NEW}
PHPSTORM_ROOT=${PHPSTORM_ROOT_NEW}
APP_FOLDER="/usr/share"

#======================================| PHPSTORM
echo ""
read -p "Do you need to re-install the PHPStorm IDE and tools? (y/N): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
echo ""
  echo "Downloading PHPStorm, please wait ..."
  sudo mkdir -p "${APP_FOLDER}/${PHPSTORM_ROOT}"
  sudo wget ${WGET_VERBOSE} -O "${APP_FOLDER}/PhpStorm.tar.gz" --referer="${REFERER}" --user-agent="${USERAGENT}" --header="${HEAD1}" --header="${HEAD2}" --header="${HEAD3}" --header="${HEAD4}" --header="${HEAD5}" "${PHPSTORM_URL}"
  sudo tar xfz "${APP_FOLDER}/PhpStorm.tar.gz" -C "${APP_FOLDER}/${PHPSTORM_ROOT}" --strip-components 1 && sudo rm "${APP_FOLDER}/PhpStorm.tar.gz"
  sudo mv "${APP_FOLDER}/PhpStorm" "${APP_FOLDER}/PhpStorm-bak"
  sudo mv "${APP_FOLDER}/${PHPSTORM_ROOT}" "${APP_FOLDER}/PhpStorm"
  cd "${APP_FOLDER}/PhpStorm/bin"
  sudo chmod 755 "${APP_FOLDER}/PhpStorm"
  sudo chmod +x ./phpstorm.sh
  bash ./phpstorm.sh

  sudo rm -rf /usr/local/bin/phpstorm
  sudo ln -s ${APP_FOLDER}/PhpStorm/bin/phpstorm.sh /usr/local/bin/phpstorm
#  sudo chmod +x /usr/local/bin/phpstorm
  sudo chmod 755 /usr/local/bin/phpstorm

  sudo chmod 755 "/usr/share/applications/jetbrains-phpstorm.desktop"

fi

echo ""
echo "Complete."
echo "This is a work in progress. Please help out where you can."
echo "Incomplete and not fully tested."

stage_finished=0
exit "$stage_finished"
