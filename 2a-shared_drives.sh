#!/bin/bash
set -e

#======================================| Import Variables
# Make sure you have edited this file
CWD="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CWD}"/config.ini
if [[ ${DEBUG} == true ]]; then set -x -v; fi

echo "Running some initial installations, please wait..."

sudo apt-get ${APTGET_VERBOSE} install build-essential ca-certificates file gnupg libnss3-tools mkcert procps

## Shared folders

PS3='Please select your Virtual environment: '
options=("VirtualBox" "VMWare" "or Quit")
select opt in "${options[@]}"
do
  case $opt in
    "VirtualBox")
      echo "you chose choice $REPLY which is $opt"
      echo "  : We will mount the shared drives as per VirtualBox."
      export INSTALL_VIRTUALBOX=true
      break
      ;;
    "VMWare")
      echo "you chose choice $REPLY which is $opt"
      echo "  : We will mount the shared drives as per VMWare."
      export INSTALL_VMWARE=true
      break
      ;;
    "or Quit")
      break
      ;;
    *) echo "invalid option $REPLY";;
  esac

done

if [[ ${INSTALL_VIRTUALBOX} == true ]]; then
# Setup shared folders between virtualbox host and virtualbox guest
# Note difference between shared and vbox-host.  That's important.  Requires reboot
#sudo sed -i 's|# By default this script does nothing.|mount -t vboxsf -o uid=1000,gid=1000 '"${HOSTSHARE} \/mnt\/${HOSTSHARE}"'|g'  /etc/rc.local
#sudo mkdir /mnt/"${HOSTSHARE}"
#sudo chmod ugo=rwX /mnt/"${HOSTSHARE}"
#ln -s "/mnt/${HOSTSHARE}" ${HOME}/Desktop/${HOSTSHARE}
#cat > "/mnt/${HOSTSHARE}/readme.txt" <<END
#If you are seeing this file, then Virtualbox shared folders are not setup correctly.
#
#1) Open the Devices menu (Virtualbox), and choose "Shared Folders..."
#2) Choose: Add Shared folder.  (Insert key)
#3) FOLDER PATH: Browse for a folder you'd like to access on the host.
#4) FOLDER NAME: ${HOSTSHARE}
#5) MAKE PERMANENT: checked, otherwise it'll create a transient folder and it won't work.
#6) Choose Ok > Choose Ok > Finally, reboot the virtual machine
#
#When completed correctly, this file will disappear and you'll have access to files on the host.
#END
#
  # VirtualBox
  # https://chewett.co.uk/blog/2610/using-virtualbox-shared-folder-on-ubuntu-20-04/#:~:text=Configuring%20Shared%20Folders%20in%20Virtualbox,folder%20by%20pressing%20the%20%2B%20button.
  echo ""
  read -p "Have you shared the '.acquia' folder for acli, Acquia CLI and aliases? (y/N): " -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo "acquia /home/${USER}/.acquia/ vboxsf defaults,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
	sudo apt-get ${APTGET_VERBOSE} update
	sudo apt-get ${APTGET_VERBOSE} install php php-xml php-curl
    cd ~
    curl -OL "https://github.com/acquia/cli/releases/latest/download/acli.phar"
    chmod +x acli.phar
    # mv acli.phar /usr/local/bin/acli
    sudo ln -s ~/acli.phar /usr/local/bin/acli
    cd -
  fi
  echo ""
  read -p "Have you shared '.aws' folder for Amazon CLI and config? (y/N): " -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo "aws /home/${USER}/.aws/ vboxsf defaults,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
  fi
  echo ""
  echo "This script adds some Certificates required by CACI and expects them to be in a 'shared' folder."
  read -p "Have you shared a 'shared' directory? (y/N): " -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo "shared /home/${USER}/shared/ vboxsf defaults,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
  fi
  echo ""
  read -p "Have you shared a 'sites' directory? (y/N): " -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo "sites /home/${USER}/sites/ vboxsf defaults,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
  fi
  echo ""
  read -p "Have you shared an '.ssh' directory to keep your keys together? (y/N): " -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo "ssh /home/${USER}/.ssh_shared/ vboxsf defaults,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
    echo "This will require you to copy and change permissions on the keys. "
    echo "After a reboot please run: ./2b-add_ssh_keys.sh."
  fi
fi

if [[ ${INSTALL_VMWARE} == true ]]; then
  # VMWare
  # https://docs.vmware.com/en/VMware-Workstation-Pro/17/com.vmware.ws.using.doc/GUID-AB5C80FE-9B8A-4899-8186-3DB8201B1758.html
  read -p "Have you shared .acquia CLI and alias directory? (y/N): " -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo ".host:/acquia /home/${USER}/.acquia/ fuse.vmhgfs-fuse defaults,allow_other,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
	sudo apt-get ${APTGET_VERBOSE} update
	sudo apt-get ${APTGET_VERBOSE} install php php-xml php-curl
    cd ~
    curl -OL "https://github.com/acquia/cli/releases/latest/download/acli.phar"
    chmod +x acli.phar
    # mv acli.phar /usr/local/bin/acli
    sudo ln -s ~/acli.phar /usr/local/bin/acli
    cd -
  fi
  echo ""
  read -p "Have you shared .aws CLI directory? (y/N): " -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo ".host:/aws /home/${USER}/.aws/ fuse.vmhgfs-fuse defaults,allow_other,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
    cd ~
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    unzip awscliv2.zip
    sudo ./aws/install
    cd -
  fi
  echo ""
  read -p "Have you shared a 'shared' directory? (y/N): " -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo ".host:/shared /home/${USER}/shared/ fuse.vmhgfs-fuse defaults,allow_other,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
    if [[ ! -f "./home/${USER}/shared/Cisco_Umbrella_Root_CA.cer" ]] ; then
      echo "File ./home/${USER}/shared/Cisco_Umbrella_Root_CA.cer does not exist, please download it and add it to the shared folder. It will be needed for accessing the internet in later steps."
      echo "Download it from: https://static.caci.co.uk/umbrella/Cisco_Umbrella_Root_CA.cer"
    fi
  fi
  echo ""
  read -p "Have you shared .ssh directory? (y/N): " -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo ""
    echo ".host:/ssh_shared /home/${USER}/.ssh_shared/ fuse.vmhgfs-fuse defaults,allow_other,uid=1000,gid=1000 0 0" | sudo tee -a /etc/fstab > /dev/null
    echo "This will require you to copy and change permissions on the keys."
    echo "After a reboot please run: ./2b-add_ssh_keys.sh."
  fi
fi

echo ""
echo "Complete. Please run:"
echo "$ sudo reboot now"
echo "When restarted, please continue with:"
echo "$ ./2b-add_ssh_keys.sh"

stage_finished=0
exit "$stage_finished"
